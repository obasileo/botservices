package ru.infsys.botservices;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class MvcConfig implements WebMvcConfigurer {

    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/")
                .setViewName("home");
        registry.addViewController("/admin/operators")
                .setViewName("/admin/operators");
        registry.addViewController("/login")
                .setViewName("login");
        registry.addViewController("/admin")
                .setViewName("admins");
        registry.addViewController("/admin/add")
                .setViewName("addadmin");

    }

}
