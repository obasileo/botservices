package ru.infsys.botservices.persistence.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import lombok.Data;

/**
 * Конфигурационные параметры модуля интеграции.
 */
@Data
@Entity
@Table(name = "configparam")
public class ConfigParam {

    /**
     * Наименование конфигурационного параметра.
     */
    @Id
    @NotNull
    @Column(name = "paramname", nullable = false)
    private String paramName;

    /**
     * Значение конфигурационного параметра.
     */
    @Column(name = "paramvalue")
    private String paramValue;
}
