package ru.infsys.botservices.persistence.entity;

import javax.persistence.Embeddable;
import javax.persistence.Id;
import lombok.Data;

@Data
@Embeddable
public class Departments {

    @Id
    private int id;

    private String name;

    private int operid;
}
