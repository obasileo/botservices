package ru.infsys.botservices.persistence.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import lombok.Data;

/**
 * Оператор.
 */
@Data
@Entity
@Table(name = "operator")
public class OperatorBkp {

    /**
     * Идентификатор Оператора.
     */
    @Id
    @NotNull
    @Column(nullable = false)
    private int id;

    /**
     * Логин Оператора в формате: «домен\логин».
     */
    @NotNull
    @Column(nullable = false)
    private String login;

    /**
     * ФИО Оператора.
     */
    @Column(name = "fullname")
    private String fullName;

    /**
     * Должность оператора.
     */
    private String position;

    /**
     * Телефоны оператора.
     */
    private String phones;

    /**
     * Массив департаментов, в которых состоит оператор.
     */
    private String departments;

    /**
     * Строковое представление фото оператора в Base64.
     */
    private String photo;

    /**
     * Статус доступности Оператора.
     */
    @NotNull
    @Column(nullable = false)
    private boolean available;

}
