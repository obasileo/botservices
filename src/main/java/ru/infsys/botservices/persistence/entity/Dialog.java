package ru.infsys.botservices.persistence.entity;

import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import lombok.Data;

/**
 * Диалог.
 */
@Data
@Entity
@Table(name = "dialog")
public class Dialog {

    /**
     * Идентификатор диалога.
     */
    @Id
    @NotNull
    @Column(nullable = false)
    private int id;

    /**
     * Идентификатор пользователя.
     */
    @NotNull
    @Column(name = "userid", nullable = false)
    private int userId;

    /**
     * Идентификатор оператора.
     */
    @Column(name = "operid")
    private Integer operId;

    /**
     * Метка времени создания сообщения.
     */
    @Column(name = "createstamp")
    private LocalDateTime createStamp;

    /**
     * Последняя активность в диалоге (1 - Пользователь, 2 - Оператор).
     */
    @NotNull
    @Column(name = "lastactivity", nullable = false)
    private int lastActivity;

    /**
     * Метка времени последней активности в диалоге.
     */
    @Column(name = "lastactivitystamp")
    private LocalDateTime lastActivityStamp;

    /**
     * Признак нахождения в очереди.
     */
    @Column(name = "inqueue")
    private boolean inQueue;

    /**
     * Идентификатор последнего сообщения в диалоге.
     */
    @Column(name = "lastmsgid")
    private int lastMsgId;

}
