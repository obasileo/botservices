package ru.infsys.botservices.persistence.entity;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;
import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotNull;
import lombok.Data;

/**
 * Базовый класс для сущностей Пользователь и Оператор (таблицы user и operator).
 */
@MappedSuperclass
@Data
public class UserBase {

    /**
     * Логин Пользователя в формате: «домен\логин».
     */
    @NotNull
    @Column(nullable = false)
    private String login;

    /**
     * ФИО Пользователя.
     */
    @Column(name = "fullname")
    private String fullName;

    /**
     * Должность Пользователя.
     */
    private String position;

    /**
     * Телефоны Пользователя.
     */
    private String phones;

    /**
     * Массив департаментов, в которых состоит пользователь.
     */
    private String departments;

    /**
     * Строковое представление фото пользователя в Base64.
     */
    private String photo;

    /**
     * Метка времени последней синхронизации с LDAP.
     */
    @Column(name = "ldapsyncstamp")
    private LocalDateTime ldapSyncStamp;

    /**
     * Преобразование списка департаментов в строку департаментов, разделенных запятой.
     * @param departments - список департаментов.
     */
    public void setDepartments(List<String> departments) {

        if (departments != null) {
            this.departments = departments.stream()
                    .map(String::valueOf)
                    .collect(Collectors.joining(","));
        }
    }
}

