package ru.infsys.botservices.persistence.entity;

import java.time.LocalDateTime;
import java.util.List;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.JoinColumn;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotNull;
import lombok.Data;

/**
 * Базовый класс для сущностей Пользователь и Оператор (таблицы user и operator).
 */
@MappedSuperclass
@Data
/*@TypeDef(
        name = "listlist",
        typeClass = ArrayType.class
)*/
public class UserBaseBkp {

    /**
     * Логин Пользователя в формате: «домен\логин».
     */
    @NotNull
    @Column(nullable = false)
    private String login;

    /**
     * ФИО Пользователя.
     */
    @Column(name = "fullname")
    private String fullName;

    /**
     * Должность Пользователя.
     */
    private String position;

    /**
     * Телефоны Пользователя.
     */
    private String phones;

    /**
     * Массив департаментов, в которых состоит пользователь.
     */
    //@Type(type = "ArrayType.class" )
    //@Column(columnDefinition = "text[]")
    @ElementCollection
    @CollectionTable(name = "departments", joinColumns = @JoinColumn(name = "operid"))
    @Column(name = "name")
    //private Set<String> phoneNumbers = new HashSet<>();
    private List<String> departments;

    /**
     * Строковое представление фото пользователя в Base64.
     */
    private String photo;

    /**
     * Метка времени последней синхронизации с LDAP.
     */
    @Column(name = "ldapsyncstamp")
    private LocalDateTime ldapSyncStamp;

    /**
     * Преобразование списка департаментов в строку департаментов, разделенных запятой.
     * @param departments - список департаментов.
     */
    /*public void setDepartments(List<String> departments) {

        this.departments = departments.stream()
                .map(n -> String.valueOf(n))
                .collect(Collectors.joining(","));
    }*/
}
