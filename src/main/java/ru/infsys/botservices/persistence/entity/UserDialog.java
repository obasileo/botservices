package ru.infsys.botservices.persistence.entity;

import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import lombok.Data;

/**
 * Пользователь.
 */
@Data
@Entity
@Table(name = "userdialog")
public class UserDialog extends UserBase {

    /**
     * Идентификатор пользователя.
     */
    @Id
    @NotNull
    @Column(name = "userid", nullable = false)
    private int id;

    /**
     * Идентификатор оператора.
     */
    @Column(name = "operid")
    private Integer operId;

    /**
     * Последняя активность в диалоге (0 – подключение, 1 - входящее сообщение, 2 - исходящее сообщение).
     */
    @NotNull
    @Column(name = "lastactivity", nullable = false)
    private int lastActivity;

    /**
     * Метка времени последней активности в диалоге.
     */
    @Column(name = "lastactivitystamp")
    private LocalDateTime lastActivityStamp;

    /**
     * Признак подключения модуля интеграции к пользователю.
     */
    @Column(name = "useron")
    private boolean userOn;

    /**
     * Признак нахождения в очереди.
     */
    @Column(name = "inqueue")
    private boolean inQueue;

    /**
     * Метка времени последнего сообщения в диалоге.
     */
    @Column(name = "lastmsgstamp")
    private LocalDateTime lastMsgStamp;

}
