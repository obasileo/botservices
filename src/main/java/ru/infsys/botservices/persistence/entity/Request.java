package ru.infsys.botservices.persistence.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import lombok.Data;

/**
 * Запрос СОУ ИТС.
 */
@Data
@Entity
@Table(name = "request")
public class Request {

    /**
     * Идентификатор запроса.
     */
    @Id
    @NotNull
    @Column(name = "reqnumber", nullable = false)
    private String id;

    /**
     * Идентификатор диалога пользователя.
     */
    @NotNull
    @Column(name = "userid", nullable = false)
    private int userId;

    /**
     * Идентификатор первого сообщения, помещенного в запрос.
     */
    @Column(name = "firstmsg")
    private Integer firstMsg;

    /**
     * Идентификатор последнего сообщения, помещенного в запрос.
     */
    @Column(name = "lastmsg")
    private Integer lastMsg;

}
