package ru.infsys.botservices.persistence.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import lombok.Data;

/**
 * Оператор.
 */
@Data
@Entity
@Table(name = "operator")
public class Operator extends UserBase {

    /**
     * Идентификатор Оператора.
     */
    @Id
    @NotNull
    @Column(name = "operid", nullable = false)
    private int id;

    @Column(name = "chatbotskill")
    private Boolean chatBotSkill;

}
