package ru.infsys.botservices.persistence.entity;

import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import lombok.Data;

/**
 * Сообщения в диалоге.
 */
@Data
@Entity
@Table(name = "message")
public class Message {

    /**
     * Идентификатор сообщения.
     */
    @Id
    @NotNull
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false)
    private Integer id;

    /**
     * Идентификатор пользователя.
     */
    @NotNull
    @Column(name = "userid", nullable = false)
    private int userId;

    /**
     * Код сообщения:
     * 0 - сообщение чат-бота пользователю;
     * 1 - сообщение пользователя чат-боту;
     * 2 - сообщение оператора пользователю;
     * 3 - сообщение пользователя оператору;
     * 4 - сообщение пользователю о подключении оператора.
     */
    @NotNull
    @Column(name = "msgcode", nullable = false)
    private int msgCode;

    /**
     * Метка времени создания сообщения.
     */
    @Column(name = "createstamp")
    private LocalDateTime createStamp;

    /**
     * Текст сообщения.
     */
    @Column(name = "msg")
    private String message;

    /**
     * Файлы.
     */
    @Column(name = "msgfile")
    private String msgFile;

    @OneToOne
    @JoinColumn(name = "operid")
    private Operator operator;

}
