package ru.infsys.botservices.persistence.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import lombok.Data;

/**
 * Пользователь.
 */
@Data
@Entity
@Table(name = "user")
public class User extends UserBase {

    @Id
    @NotNull
    @Column(name = "userid", nullable = false)
    private int id;

}
