package ru.infsys.botservices.persistence.repository;

import java.time.LocalDateTime;
import java.util.List;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;
import ru.infsys.botservices.persistence.entity.UserDialog;

public interface UserDialogRepo extends CrudRepository<UserDialog, Integer> {

    /**
     * Поиск всех диалогов в которых участвует оператор
     *
     * @param operId - Идентификатор Оператора
     * @return - возвращает все диалоги в которых участвует Оператор
     */
    List<UserDialog> findAllByOperId(int operId);

    /**
     * Поиск диалога по его идентификатору
     *
     * @param userId - Идентификатор пользователя.
     * @return - возвращает диалог
     */
    UserDialog findUserDialogById(int userId);

    /**
     * Поиск текущих диалогов оператора.
     * Выполняется поиск всех диалогов Оператора в которых inQueue = false AND userOn = true.
     *
     * @param operId - Идентификатор Оператора
     * @return - возвращает список текущих диалогов Оператора
     */
    @Query("SELECT d FROM UserDialog d WHERE d.operId = ?1 AND d.inQueue = false AND d.userOn = true"
            + " ORDER BY d.lastActivityStamp")
    List<UserDialog> findCurrentDialogs(int operId);

    /**
     * Отправить в очередь все диалоги, к которым подключен оператор.
     * (установить атрибут inQueue = true для всех диалогов пользователя, у которых operId = OperState.operId).
     */
    @Modifying
    @Transactional
    @Query("UPDATE UserDialog d SET d.inQueue = true, d.operId = null WHERE d.operId = ?1")
    void moveOperDialogsToQueue(int operId);

    /**
     * Устанавливает признак подключения модуля интеграции к пользователю (userOn = true)
     * для всех пользователей списка у которых данный признак false.
     * @param userIdList - список идентификаторов пользователей
     */
    @Modifying
    @Transactional
    @Query("UPDATE UserDialog d SET d.userOn = true, d.inQueue = true WHERE d.id IN ?1")
    void connectUsers(List<Integer> userIdList);

    /**
     * Устанавливает признак подключения модуля интеграции к пользователю (userOn = true)
     *
     * @param userId - идентификатор пользователя (диалога)
     */
    @Modifying
    @Transactional
    @Query("UPDATE UserDialog d SET d.userOn = true WHERE d.id = ?1")
    void connectUser(int userId);

    /**
     * Поиск всех диалогов у которых наступило время синхронизации
     * (время последней синхронизации (ldapSyncStamp) меньше или равно времени синхронизации и
     * временя последней активности в диалоге больше или равно времени синхронизации).
     * @param syncTime - время синхронизации (Текущее время - sysconfig.TokenLifeLimit)
     * @return - возвращает список диалогов у которых наступило время синхронизации.
     */
    @Query("SELECT ud FROM UserDialog ud WHERE ud.ldapSyncStamp <= ?1 AND ud.lastActivityStamp >= ?1")
    List<UserDialog> findUserDialogByLdapSyncStampAndLastActivityStamp(LocalDateTime syncTime);

    /**
     * Поиск всех диалогов у которых признак подключения модуля интеграции к пользователю = true
     * и признак нахождения в очереди = true
     * @return - возвращает список диалогов
     */
    List<UserDialog> findUserDialogsByUserOnTrueAndInQueueTrue();

    /**
     * Поиск неактивных диалогов (время последней активности в диалоге меньше времени отключения)
     * Время отключения рассчитывается как
     * Текущее время - UserAnswerTimeout или Текущее время - OperAnswerTimeout.
     *
     * Диалог в котором последняя активность принадлежит пользователю (lastActivity = 1)
     *   считается неактивным со стороны оператора.
     * Диалог в котором последняя активность принадлежит оператору (lastActivity = 2)
     *   считается неактивным со стороны пользователя.
     * @param lastActivity - кому принадлежит последняя активность в диалоге
     *                     1 - пользователь
     *                     2 - оператор
     * @param disconnectTime - время отключения пользователя
     * @return - возвращает список диалогов
     */
    @Query("SELECT ud FROM UserDialog ud "
            + "WHERE ud.userOn = true "
            + "AND ud.lastActivity = ?1 AND ud.lastActivityStamp <= ?2")
    List<UserDialog> findInactiveUserDialogs(int lastActivity, LocalDateTime disconnectTime);

    /**
     * Поиск всех диалогов
     */
    @Query("SELECT ud FROM UserDialog ud")
    List<UserDialog> findAll();

    /**
     * При отключении пользователя устанавливает атрибуты диалога
     * operId = null, lastActivity = 0, userOn = false, inQueue = false
     *
     * @param userId - Идентификатор пользователя (диалога).
     */
    @Modifying
    @Transactional
    @Query("UPDATE UserDialog d "
            + "SET d.operId = null, d.lastActivity = 0, d.userOn = false, d.inQueue = false WHERE d.id = ?1")
    void cancelUser(Integer userId);

    /**
     * Поиск всех активных диалогов (распределенных на операторов)
     * @return
     */
    List<UserDialog> findByUserOnTrueAndInQueueFalse();

}
