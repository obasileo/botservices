package ru.infsys.botservices.persistence.repository;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import ru.infsys.botservices.persistence.entity.ConfigParam;

public interface ConfigParamRepo extends CrudRepository<ConfigParam, String> {

    /**
     * Поиск по нескольким наименованиям конфигурацинных параметров (оператор IN ('param1', 'param2', ...))
     *
     * @param list - список наименований конфигурационных параметров
     * @return - возвращает список конфигурационных параметров
     */
    List<ConfigParam> findByParamNameIn(List<String> list);

    /**
     * Поиск по наименованию кофигурационного параметра.
     *
     * @param paramName - наименование конфигурационнго параметра
     * @return - возвращает конфигурационный параметр
     */
    ConfigParam findByParamName(String paramName);

    List<ConfigParam> findAll();

}
