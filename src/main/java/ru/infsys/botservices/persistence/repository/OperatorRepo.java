package ru.infsys.botservices.persistence.repository;

import java.time.LocalDateTime;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import ru.infsys.botservices.persistence.entity.Operator;

public interface OperatorRepo extends CrudRepository<Operator, Integer> {

    /**
     * Поиск оператора по его идентификатору
     *
     * @param id - Идентификатор Оператора
     * @return - возвращает Оператора
     */
    Operator findById(int id);

    /**
     * Поиск оператора по его логину
     *
     * @param login - Логин Оператора в формате «домен\логин»
     * @return - возвращает Оператора
     */
    @Query("SELECT o from Operator o WHERE LOWER(o.login) = LOWER(?1)")
    Operator findByLogin(String login);

    @Query("SELECT o from Operator o WHERE LOWER(o.login) LIKE LOWER(?1)")
    Operator findLikeLogin(String login);

    Operator findByLoginContainingIgnoreCase(String login);

    /**
     * Поиск всех операторов у которых наступило время синхронизации
     * (время последней синхронизации (ldapSyncStamp) меньше или равно времени синхронизации).
     * @param syncTime - время синхронизации (Текущее время - sysconfig.TokenLifeLimit)
     * @return - возвращает список операторов у которых наступило время синхронизации.
     */
    List<Operator> findOperatorByLdapSyncStampLessThanEqual(LocalDateTime syncTime);

    List<Operator> findAll();

}
