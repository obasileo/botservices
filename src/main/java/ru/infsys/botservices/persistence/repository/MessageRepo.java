package ru.infsys.botservices.persistence.repository;

import java.time.LocalDateTime;
import java.util.List;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import ru.infsys.botservices.persistence.entity.Message;

public interface MessageRepo extends CrudRepository<Message, Integer> {

    /**
     * Поиск сообщений по Идентификатору пользователя(диалога).
     * Количество возвращаемых записей может быть задано параметром msgLimit из MsgRequestInfo,
     * Количество возвращаемых записей в любом случае ограничено конфигурационным параметром msgMaxCount
     * из таблицы конфигурации ConfigParam.
     * @param userId - Идентификатор пользователя (диалога).
     * @param pageable - Использовать реализацию PageRequest для ограничения количества возвращаемых записей.
     * @return - возвращает список сообщений заданного диалога, количество возвращаемых записей ограничено Pageable.
     */
    @Query("SELECT m FROM Message m WHERE m.userId = ?1 ORDER BY m.createStamp DESC")
    List<Message> findMessagesByUserId(int userId, Pageable pageable);

    /**
     * Поиск сообщений по Идентификатору пользователя(диалога) и по времени, начиная с которого выдать сообщения.
     * Количество возвращаемых записей может быть задано параметром msgLimit из MsgRequestInfo,
     * Количество возвращаемых записей в любом случае ограничено конфигурационным параметром msgMaxCount
     * из таблицы конфигурации ConfigParam.
     * @param userId - Идентификатор пользователя (диалога).
     * @param firstMsgStamp - Метка времени первого сообщения из группы выдаваемых сообщений.
     * @param pageable - Использовать реализацию PageRequest для ограничения количества возвращаемых записей.
     * @return - возвращает список сообщений заданного диалога, количество возвращаемых записей ограничено Pageable.
     */
    @Query("SELECT m FROM Message m WHERE m.userId = ?1 AND m.createStamp >= ?2 ORDER BY m.createStamp DESC")
    List<Message> findMessagesByUserIdAndFirstCreateStamp(
            int userId, LocalDateTime firstMsgStamp, Pageable pageable);

    /**
     * Поиск сообщений по Идентификатору пользователя(диалога) и по интервалу времени.
     * Сообщения будут выданы из указанного интервала.
     * Количество возвращаемых записей может быть задано параметром msgLimit из MsgRequestInfo,
     * Количество возвращаемых записей в любом случае ограничено конфигурационным параметром msgMaxCount
     * из таблицы конфигурации ConfigParam.
     * @param userId - Идентификатор пользователя (диалога).
     * @param firstMsgStamp - Метка времени первого сообщения из группы выдаваемых сообщений.
     * @param lastMsgStamp - Метка времени последнего сообщения из группы выдаваемых сообщений.
     * @param pageable - Использовать реализацию PageRequest для ограничения количества возвращаемых записей.
     * @return - возвращает список сообщений заданного диалога, количество возвращаемых записей ограничено Pageable.
     */
    @Query("SELECT m FROM Message m WHERE m.userId = ?1 "
            + "AND m.createStamp BETWEEN ?2 AND ?3 "
            + "ORDER BY m.createStamp DESC")
    List<Message> findMessagesByUserIdAndBetweenCreateStamp(
            int userId, LocalDateTime firstMsgStamp, LocalDateTime lastMsgStamp, Pageable pageable);

    /**
     * Поиск сообщения с наибольшим временем создания
     * @param userId - Идентификатор пользователя (диалога).
     * @return - возвращает наибольшее время создания сообщения в указанном диалоге
     */
    @Query("SELECT MAX(m.createStamp) FROM Message m WHERE m.userId = ?1")
    LocalDateTime findMaxCreateStamp(int userId);
}
