package ru.infsys.botservices.persistence.repository;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import ru.infsys.botservices.persistence.entity.Admin;

public interface AdminRepo extends CrudRepository<Admin, Integer> {

    /**
     * Поиск администратора по его идентификатору
     *
     * @param id - Идентификатор администратора
     * @return - возвращает администратора
     */
    Admin findById(int id);

    List<Admin> findAll();



    /**
     * Поиск оператора по его логину
     *
     * @param login - Логин Оператора в формате «домен\логин»
     * @return - возвращает Оператора
     *//*
    @Query("SELECT o from Operator o WHERE LOWER(o.login) = LOWER(?1)")
    Operator findByLogin(String login);

    *//**
     * Поиск всех операторов у которых наступило время синхронизации
     * (время последней синхронизации (ldapSyncStamp) меньше или равно времени синхронизации).
     * @param syncTime - время синхронизации (Текущее время - sysconfig.TokenLifeLimit)
     * @return - возвращает список операторов у которых наступило время синхронизации.
     *//*
    List<Operator> findOperatorByLdapSyncStampLessThanEqual(LocalDateTime syncTime);*/



}
