package ru.infsys.botservices.persistence.repository;

import org.springframework.data.repository.CrudRepository;
import ru.infsys.botservices.persistence.entity.Request;

public interface RequestRepo extends CrudRepository<Request, String> {


    /**
     * Поиск запроса СОУ ИТС по его идентификатору.
     *
     * @param id - идентификатор запроса СОУ ИТС
     * @return - возвращает запрос СОУ ИТС
     */
    Request findRequestById(String id);

}
