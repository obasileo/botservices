package ru.infsys.botservices.swagger;

import java.util.Collections;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Tag;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    public static final String TAG_OPERATOR = "Operator";
    public static final String TAG_CONFIG = "Config";
    public static final String TAG_DIALOG = "Dialog";

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("ru.infsys.botservices"))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(apiInfo())
                .tags(new Tag(TAG_OPERATOR, "Сервисы подключения оператора и изменения статуса оператора"),
                        new Tag(TAG_CONFIG, "Сервисы конфигурирования "),
                        new Tag(TAG_DIALOG, "Сервисы ведения диалогов ")
                );
    }

    private ApiInfo apiInfo() {

        return new ApiInfo(
                "BOTSERVICES REST API",
                "",
                "v1",
                "",
                new Contact("", "", ""),
                "", "", Collections.emptyList());
    }
}
