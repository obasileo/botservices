package ru.infsys.botservices.utils;

import java.nio.charset.StandardCharsets;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.tomcat.util.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Класс для шифрования/дешифрования алгоритмом AES-256-CBC.
 * При шифровании AES-256 в качестве ключа берется sha-256 от пароля,
 * в качестве IV берется сам пароль в виде массива байт.
 */

public class Crypto {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private static final String ALGO = "AES/CBC/PKCS5Padding";

    private Cipher cipher;
    private SecretKeySpec secretKey;
    private IvParameterSpec ivParameterSpec;

    /**
     * Подготовка необходимых объектов для методов шифрования/дешифрования.
     * SecretKeySpec - создает секретный ключ из заданного байтового массива.
     * Cipher - класс обеспечивает функциональность криптографического шифра для шифрования и дешифрования.
     * IvParameterSpec - определяет вектор инициализации (IV).
     *
     * @param password - пароль для шифрования
     */
    private void prepareCrypto(String password) throws Exception {

        secretKey = new SecretKeySpec(DatatypeConverter
                .parseHexBinary(DigestUtils.sha256Hex(password)), "AES");

        ivParameterSpec = new IvParameterSpec(password.getBytes(StandardCharsets.UTF_8));

        cipher = Cipher.getInstance(ALGO);
    }

    /**
     * Шифрует строку алгоритмом AES-256-CBC.
     *
     * @param password - пароль для шифрования
     * @param stringToEncrypt - шифруемая строка
     * @return - возвращает зашифрованную строку
     */
    public String encryption(String password, String stringToEncrypt) throws Exception {

        try {
            prepareCrypto(password);

            cipher.init(Cipher.ENCRYPT_MODE, secretKey, ivParameterSpec);

            return new String(Base64.encodeBase64(cipher.doFinal(stringToEncrypt.getBytes())), "UTF-8");

        } catch (Exception e) {
            logger.error("При шифровании строки: {} произошла ошибка: {}",stringToEncrypt, e.getMessage());
            throw e;
        }
    }

    /**
     * Дешифрует строку алгоритмом AES-256-CBC.
     *
     * @param password - пароль для шифрования
     * @param stringToDecrypt - дешифруемая строка
     * @return - возвращает дешифрованную строку
     */
    public String decryption(String password, String stringToDecrypt) throws Exception {

        try {
            prepareCrypto(password);

            cipher.init(Cipher.DECRYPT_MODE, secretKey, ivParameterSpec);

            return new String(cipher.doFinal(Base64.decodeBase64(stringToDecrypt.getBytes())), "UTF-8");

        } catch (Exception e) {
            logger.error("При дешифровании строки: {} произошла ошибка: {}",stringToDecrypt, e.getMessage());
            throw e;
        }
    }
}
