package ru.infsys.botservices.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.TimeZone;

/**
 * Класс содержит методы по преобразованию даты/времени в необходимые форматы.
 */
public class DateTimeConverter {

    /**
     * Возвращает строку в формате "yyyy-MM-dd HH:mm:ss" от текущих даты/времени без учета таймзоны (UTC).
     *
     * @return
     */
    public static String getUtcDateTimeString() {

        return LocalDateTime.now(ZoneId.of("UTC")).format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
    }

    /**
     * Возвращает Timestamp в миллисекундах без учета таймзоны (UTC)
     * от строки в формате "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'".
     *
     * @return
     */
    public static long getUtcDateTimeFromString(String dateInString) throws ParseException {

        return LocalDateTime.parse(dateInString, DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"))
                .atZone(ZoneId.of("UTC")).toInstant().toEpochMilli();
    }

    /**
     * Возвращает строку в формате "yyyy-MM-dd HH:mm:ss" от даты/времени в миллисекундах без учета таймзоны (UTC).
     *
     * @param millis - дата/время в миллисекундах
     * @return
     */
    public static String getUtcDateTimeStringFromMillis(Long millis) {

        return Instant.ofEpochMilli(millis).atZone(ZoneId.of("UTC"))
                .format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
    }
}
