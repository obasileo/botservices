package ru.infsys.botservices;

import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

@EnableScheduling
@SpringBootApplication
public class BotservicesApplication {

    public static void main(String[] args) {

        SpringApplication.run(BotservicesApplication.class, args);

    }

    @Bean
    public ModelMapper mapper() {
        return new ModelMapper();
    }

    /**
     * Конфигурирование пула потоков TaskScheduler
     * https://www.baeldung.com/spring-task-scheduler
     */
    @Bean
    public ThreadPoolTaskScheduler threadPoolTaskScheduler() {
        ThreadPoolTaskScheduler threadPoolTaskScheduler
                = new ThreadPoolTaskScheduler();
        threadPoolTaskScheduler.setPoolSize(5);
        return threadPoolTaskScheduler;
    }

}
