package ru.infsys.botservices.controller;

import static ru.infsys.botservices.swagger.SwaggerConfig.TAG_CONFIG;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import java.util.List;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.infsys.botservices.service.espp.IntegrationService;
import ru.infsys.botservices.service.espp.model.ConfigParamInfo;

/**
 * Чтение и запись конфигурационных параметров модуля интеграции.
 * Параметры хранятся в таблице configparam.
 */
@RestController
@RequestMapping("/chat/config/")
@Api(tags = TAG_CONFIG)
public class ConfigRest {

    @Autowired
    IntegrationService integrationService;

    @ApiOperation(value = "Прочитать значения конфигурационных параметров")
    @PostMapping("read")
    public List<ConfigParamInfo> read(
            @ApiParam(value = "Список наименований конфигурационных параметров (наименования отделяются запятыми)",
                      example = "dlgMaxCount, msgMaxCount}",
                      required = true)
            @RequestBody String params,
            @ApiParam(value = "Токен передается в Cookie \"Authorization\"", name = "Authorization")
            @CookieValue(value = "Authorization") String token) {

        return integrationService.getConfigParams(params, UUID.fromString(token));

    }

    @ApiOperation(value = "Устанавливает значения заданных конфигурационных параметров")
    @PostMapping("write")
    public void write(
            @ApiParam(value = "Массив конфигурационных параметров", required = true)
            @RequestBody List<ConfigParamInfo> configParamInfoList,
            @ApiParam(value = "Токен передается в Cookie \"Authorization\"", name = "Authorization")
            @CookieValue(value = "Authorization") String token) {

        integrationService.writeConfigParam(configParamInfoList, UUID.fromString(token));

    }
}
