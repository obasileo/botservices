package ru.infsys.botservices.controller;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.infsys.botservices.AppConfig;
import ru.infsys.botservices.persistence.entity.Operator;
import ru.infsys.botservices.persistence.entity.UserDialog;
import ru.infsys.botservices.persistence.repository.OperatorRepo;
import ru.infsys.botservices.persistence.repository.UserDialogRepo;
import ru.infsys.botservices.service.chatbot.Auth;
import ru.infsys.botservices.service.chatbot.ChatbotService;
import ru.infsys.botservices.service.espp.IntegrationService;
import ru.infsys.botservices.service.espp.model.ActiveUserDialogInfo;
import ru.infsys.botservices.service.espp.model.OperState;
import ru.infsys.botservices.service.websocket.WebSocketService;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping("/test")
public class TestController {

    @Autowired
    Auth auth;

    @Autowired
    IntegrationService integrationService;

    @Autowired
    OperatorRepo operatorRepo;

    @Autowired
    ChatbotService chatbotService;

    @Autowired
    UserDialogRepo userDialogRepo;

    @Autowired
    WebSocketService webSocketService;

    @Autowired
    AppConfig appConfig;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    /*@Autowired
    ScheduledTasks scheduledTasks;*/

    /*@Autowired
    UserRepo userRepo;*/
    /*@Autowired
    Crypto crypto;
    @Autowired
    Token token;*/
    /*@Autowired
    UserAdInfoClient userAdInfoClient;*/

    @ApiIgnore
    @GetMapping
    public String jsonString() throws Exception {


        return chatbotService.changeQueue("espp", 887);
        //LocalDateTime localDateTime = LocalDateTime.now(ZoneId.of("UTC")).minusMonths(1);
        //return chatbotService.getUserAdInfo("vip\\svc-chatbot-test-198", false);
        //scheduledTasks.allocDialog();
        //return integrationService.checkToken(UUID.fromString("123"));
        //return operatorRepo.findById(1002);
        //String tokenUnparsedJson = auth.getToken();
        //return tokenUnparsedJson;
        //return auth.getToken();
        //token.init( tokenUnparsedJson );

        //UserAd userAd = userAdInfoClient.getUserAdInfo( "REGION\\65IvanovIS_test" );

        //return userAd.getDepartments();

        /*return token.getToken() + " " +token.getIp() + " " + token.getLogin() + " " + token.getOperatorId() + " " +
        token.getOperatorType() + " " + token.getStart() + " " + token.getEnd() + " " +
        token.getStartTimestamp() + " " + token.getEndTimestamp();*/

    }

    @GetMapping("/resetdlg")
    public String resetDlg() throws Exception {

        List<UserDialog> userDialogList = userDialogRepo.findAll();

        for (UserDialog userDialog : userDialogList) {

            //Если включен фильтр, то пропускать обработку пользователей
            //с логином не соответствующему шаблону
            if (appConfig.isUseFilter()
                    && !Pattern.compile(appConfig.getLoginPattern())
                    .matcher(userDialog.getLogin()).find()) {

                continue;
            }

            /*userDialog.setUserOn(true);
            userDialog.setInQueue(true);
            userDialog.setOperId(null);*/
            userDialog.setUserOn(false);
            userDialog.setInQueue(false);
            userDialog.setOperId(null);
            if (userDialog.getLastActivityStamp() == null) {
                userDialog.setLastActivityStamp(
                        LocalDateTime.ofEpochSecond(0,0, ZoneOffset.UTC));
            }
            userDialogRepo.save(userDialog);
            try {
                chatbotService.cancelUser(userDialog.getId());
            } catch (Exception e) {
                logger.error("resetdlg: {}", e.getStackTrace());
            }
        }
        integrationService.getOperStateMapById().clear();
        integrationService.getOperStateMapByToken().clear();

        return "Ok";
    }

    @GetMapping("/operstate")
    public List<OperState> getOperState() throws Exception {

        //List<List<OperState>> list = new ArrayList<>(integrationService.getOperStateMapById().values());

        return new ArrayList<>(integrationService.getOperStateMapById().values());

    }

    @GetMapping("/init")
    public void init() throws Exception {

        webSocketService.reconnect();

    }

    @GetMapping("/dlginfo")
    public List<ActiveUserDialogInfo> dlgInfo() throws Exception {

        List<UserDialog> userDialogList = userDialogRepo.findByUserOnTrueAndInQueueFalse();

        logger.debug("Список диалогов UserOnTrueAndInQueueFalse:");

        for (UserDialog userDialog : userDialogList) {
            logger.debug("userId: {}; login: {}; operId: {}; LastActivity: {}; LastActivityStamp: {}; "
                            + "UserOn: {}; InQueue: {}; LastMsgStamp: {};",
                    userDialog.getId(),
                    userDialog.getLogin(),
                    userDialog.getOperId(),
                    userDialog.getLastActivity(),
                    userDialog.getLastActivityStamp(),
                    userDialog.isUserOn(),
                    userDialog.isInQueue(),
                    userDialog.getLastMsgStamp()
            );
        }

        List<ActiveUserDialogInfo> activeUserDialogInfoList = new ArrayList<>();

        for (UserDialog userDialog : userDialogList) {
            ActiveUserDialogInfo activeUserDialogInfo = new ActiveUserDialogInfo();
            if (userDialog.getOperId() != null) {
                Operator operator = operatorRepo.findById((int) userDialog.getOperId());
                activeUserDialogInfo.setId(userDialog.getId());
                activeUserDialogInfo.setLogin(userDialog.getLogin());
                activeUserDialogInfo.setFullName(userDialog.getFullName());
                activeUserDialogInfo.setOperatorLogin(operator.getLogin());
                activeUserDialogInfo.setOperatorName(operator.getFullName());

                activeUserDialogInfoList.add(activeUserDialogInfo);
            }
        }
        return activeUserDialogInfoList;

    }

    @GetMapping("/dlginfo2")
    public List<UserDialog> dlgInfo2() throws Exception {

        List<UserDialog> userDialogList = userDialogRepo.findByUserOnTrueAndInQueueFalse();

        logger.debug("Список диалогов UserOnTrueAndInQueueFalse:");
        logger.debug("LoginPattern: {}", appConfig.getLoginPattern());

        for (UserDialog userDialog : userDialogList) {
            logger.debug("userId: {}; login: {}; operId: {}; LastActivity: {}; LastActivityStamp: {}; "
                            + "UserOn: {}; InQueue: {}; LastMsgStamp: {};",
                    userDialog.getId(),
                    userDialog.getLogin(),
                    userDialog.getOperId(),
                    userDialog.getLastActivity(),
                    userDialog.getLastActivityStamp(),
                    userDialog.isUserOn(),
                    userDialog.isInQueue(),
                    userDialog.getLastMsgStamp()
            );
        }
        return userDialogList;
    }
}
