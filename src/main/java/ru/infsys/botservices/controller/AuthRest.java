package ru.infsys.botservices.controller;

import static ru.infsys.botservices.swagger.SwaggerConfig.TAG_OPERATOR;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import java.util.UUID;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.infsys.botservices.service.espp.IntegrationService;
import ru.infsys.botservices.service.espp.model.OperatorExtInfo;

@RestController
@RequestMapping("/chat/auth/")
@Api(tags = TAG_OPERATOR)
public class AuthRest {

    @Autowired
    IntegrationService integrationService;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @PostMapping(value = "login", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Вход оператора в систему")
    public OperatorExtInfo login(
            @ApiParam(value = "Логин Оператора в формате: «домен\\логин»",
                    example = "region\\IvanovAE")
            @RequestBody String login, HttpServletResponse response) {

        logger.info("Попытка входа пользователя: {} ", login);

        UUID token = UUID.randomUUID();

        OperatorExtInfo operatorExtInfo = integrationService.loginOperator(login.trim(), token);

        Cookie cookie = new Cookie("Authorization",
                integrationService.getOperStateMapById()
                        .get(operatorExtInfo.getId())
                        .getToken().toString());

        cookie.setPath("/");
        response.addCookie(cookie);

        logger.info("Результат входа пользователя: login: {}, id: {} ",
                operatorExtInfo.getLogin(), operatorExtInfo.getId());

        return operatorExtInfo;
    }

    @PostMapping("logout")
    @ApiOperation(value = "Выход оператора из системы")
    public void logout(@CookieValue(value = "Authorization") String token) {

        integrationService.logoutOperator(UUID.fromString(token));

    }
}
