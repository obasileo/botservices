package ru.infsys.botservices.controller;

import static ru.infsys.botservices.swagger.SwaggerConfig.TAG_OPERATOR;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.infsys.botservices.service.espp.IntegrationService;
import ru.infsys.botservices.service.espp.model.OperatorBaseInfo;
import ru.infsys.botservices.service.espp.model.OperatorSkill;

@RestController
@RequestMapping("chat/operator/")
@Api(tags = TAG_OPERATOR)
public class OperatorRest {

    @Autowired
    IntegrationService integrationService;

    @ApiOperation(value = "Установить статус доступности Оператора")
    @PostMapping("available")
    public void operatorStatus(
            @ApiParam(value = "Статус доступности (true – доступен, false - занят)",
                      example = "true",
                      required = true)
            @RequestBody boolean status,
            @ApiParam(value = "Токен передается в Cookie \"Authorization\"", name = "Authorization")
            @CookieValue(value = "Authorization") String token) {
        integrationService.changeOperatorStatus(UUID.fromString(token), status);

    }

    @ApiOperation(value = "Получить атрибуты заданного оператора")
    @PostMapping("info")
    public OperatorBaseInfo info(
            @ApiParam(value = "Идентификатор оператора", example = "101", required = true)
            @RequestBody int id,
            @ApiParam(value = "Токен передается в Cookie \"Authorization\"", name = "Authorization")
            @CookieValue(value = "Authorization") String token) {
        return  integrationService.getOperatorAttr(UUID.fromString(token), id);
    }

    @ApiOperation(value = "Получить навыки заданного оператора")
    @PostMapping("skill")
    public OperatorSkill skill(
            @ApiParam(value = "Идентификатор оператора", example = "101", required = true)
            @RequestBody int id,
            @ApiParam(value = "Токен передается в Cookie \"Authorization\"", name = "Authorization")
            @CookieValue(value = "Authorization") String token) {
        return  integrationService.getOperatorSkill(UUID.fromString(token), id);
    }
}

