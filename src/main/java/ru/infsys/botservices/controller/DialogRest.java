package ru.infsys.botservices.controller;

import static ru.infsys.botservices.swagger.SwaggerConfig.TAG_DIALOG;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import java.util.List;
import java.util.UUID;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import ru.infsys.botservices.service.chatbot.ChatbotService;
import ru.infsys.botservices.service.chatbot.model.dialog.DataForChangeQueue;
import ru.infsys.botservices.service.chatbot.model.dialog.FileRequestInfo;
import ru.infsys.botservices.service.espp.IntegrationService;
import ru.infsys.botservices.service.espp.model.MessageInfo;
import ru.infsys.botservices.service.espp.model.MsgRequestInfo;
import ru.infsys.botservices.service.espp.model.MsgToSendInfo;
import ru.infsys.botservices.service.espp.model.RequestInfo;
import ru.infsys.botservices.service.espp.model.UserDialogInfo;

@RestController
@RequestMapping("chat/dialog/")
@Api(tags = TAG_DIALOG)
public class DialogRest {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    IntegrationService integrationService;

    @Autowired
    ChatbotService chatbotService;

    @ApiOperation(value = "Выдает список диалогов, в которых участвует Оператор")
    @PostMapping("dlglist")
    public List<UserDialogInfo> dlgList(
            @ApiParam(value = "Токен передается в Cookie \"Authorization\"", name = "Authorization")
            @CookieValue(value = "Authorization") String token) {
        return integrationService.getDialogList(UUID.fromString(token));

    }

    @ApiOperation(value = "Отправить диалог в очередь")
    @PostMapping("toqueue")
    public void dialogToQueue(
            @ApiParam(value = "Идентификатор диалога", example = "101", required = true)
            @RequestBody int id,
            @ApiParam(value = "Токен передается в Cookie \"Authorization\"", name = "Authorization")
            @CookieValue(value = "Authorization") String token) {

        logger.debug("Вызов REST toqueue, dialogId = {}", id);
        integrationService.dialogToQueue(UUID.fromString(token), id);

    }

    @ApiOperation(value = "Завершить диалог")
    @PostMapping("cancel")
    public void cancel(
            @ApiParam(value = "Идентификатор диалога", example = "101", required = true)
            @RequestBody int id,
            @ApiParam(value = "Токен передается в Cookie \"Authorization\"", name = "Authorization")
            @CookieValue(value = "Authorization") String token) {
        integrationService.cancelDialog(UUID.fromString(token), id);

        logger.debug("rest Завершить диалог: " + id);

    }

    @ApiOperation(value = "Получить список сообщений заданного диалога")
    @PostMapping("msglist")
    public List<MessageInfo> msgList(
            @RequestBody MsgRequestInfo msgRequestInfo,
            @ApiParam(value = "Токен передается в Cookie \"Authorization\"", name = "Authorization")
            @CookieValue(value = "Authorization") String token) {

        List<MessageInfo> messageInfoList = integrationService.getMessageList(UUID.fromString(token), msgRequestInfo);

        //Добавил для отладки. Иногда в диалоге вдруг появляется старое сообщение.
        // Чтобы понять, это я в сервисе неправильно отдаю, либо это на стороне клиента.
        logger.debug(messageInfoList.toString());

        return messageInfoList;

    }

    @ApiOperation(value = "Отправить сообщение")
    @PostMapping("msgsend")
    public void msgSend(
            @RequestBody MsgToSendInfo msgToSendInfo,
            @ApiParam(value = "Токен передается в Cookie \"Authorization\"", name = "Authorization")
            @CookieValue(value = "Authorization") String token) {

        integrationService.sendMessage(UUID.fromString(token), msgToSendInfo);
    }

    @ApiOperation(value = "Перевести диалог в другую очередь (ДКП <-> ЕСПП).")
    @PostMapping("change_queue")
    public void msgSend(
            @RequestBody DataForChangeQueue dataForChangeQueue,
            @ApiParam(value = "Токен передается в Cookie \"Authorization\"", name = "Authorization")
            @CookieValue(value = "Authorization") String token) {

        integrationService.changeQueue(UUID.fromString(token),
                dataForChangeQueue.getTransferTo(),
                dataForChangeQueue.getUserId());
    }



    @ApiOperation(value = "Зарегистрировать запрос СОУ ИТС")
    @PostMapping("request")
    public void request(
            @RequestBody RequestInfo requestInfo,
            @ApiParam(value = "Токен передается в Cookie \"Authorization\"", name = "Authorization")
            @CookieValue(value = "Authorization") String token) {

        integrationService.regRequest(UUID.fromString(token), requestInfo);
    }

    @ApiOperation(value = "Получить файл")
    @PostMapping(value = "file", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public @ResponseBody byte[] file(
            @RequestBody FileRequestInfo fileRequestInfo,
            @ApiParam(value = "Токен передается в Cookie \"Authorization\"", name = "Authorization")
            @CookieValue(value = "Authorization") String token) throws Exception {

        //return chatbotService.getFile(fileRequestInfo);
        return IOUtils.toByteArray(chatbotService.getFile(fileRequestInfo));
    }

    @ApiOperation(value = "Получить файл ТЕСТ")
    @GetMapping(value = "file", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public @ResponseBody byte[] getFile() throws Exception {

        FileRequestInfo fileRequestInfo = new FileRequestInfo();
        fileRequestInfo.setFileId(441);
        fileRequestInfo.setFileName("2.txt");
        fileRequestInfo.setUpdateDate("1de8fa7a26ef1c3347a558ae38ff3b8ba7ce1bb0e5c894d9fc31634f31326eedf6abb5e546"
                + "c9d8249f40d0541bf55cb0b1beeb6ab998b78ee2058e0dce646883");
        fileRequestInfo.setSourceType(3);

        return IOUtils.toByteArray(chatbotService.getFile(fileRequestInfo));
    }

    /*
    @GetMapping(
  value = "/get-file",
  produces = MediaType.APPLICATION_OCTET_STREAM_VALUE
)
public @ResponseBody byte[] getFile() throws IOException {
    InputStream in = getClass()
      .getResourceAsStream("/com/baeldung/produceimage/data.txt");
    return IOUtils.toByteArray(in);
}
     */
}
