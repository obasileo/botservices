package ru.infsys.botservices.controller;

import java.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.infsys.botservices.job.ScheduledTasks;
import ru.infsys.botservices.persistence.repository.MessageRepo;
import ru.infsys.botservices.service.chatbot.Auth;
import ru.infsys.botservices.service.chatbot.ChatbotService;
import ru.infsys.botservices.service.chatbot.DialogService;
import ru.infsys.botservices.service.espp.IntegrationService;
import springfox.documentation.annotations.ApiIgnore;

@ApiIgnore
@RestController
@RequestMapping("/test2")
public class TestController2 {

    @Autowired
    Auth auth;

    @Autowired
    ChatbotService chatbotService;

    @Autowired
    DialogService dialogService;

    /*@Autowired
    ScheduledTasks scheduledTasks;*/

    @Autowired
    IntegrationService integrationService;

    @Autowired
    MessageRepo messageRepo;

    /*@Autowired
    Crypto crypto;
    @Autowired
    Token token;*/
    /*@Autowired
    UserAdInfoClient userAdInfoClient;*/

    @GetMapping
    public LocalDateTime jsonString() throws Exception {

        return messageRepo.findMaxCreateStamp(889);
        //integrationService.getConnectedUsersQueue().add(889);
        //scheduledTasks.connectUserToIntegrationModule();


        //return null;
        //return chatbotService.acceptUser(889, 0);
        //return dialogService.returnUser(889);
        //return chatbotService.cancelUser(889);
        //return chatbotService.sendMessageToUser(889, "Добрый день!");

        //String tokenUnparsedJson = auth.getToken();
        //return tokenUnparsedJson;
        //return auth.getToken();
        //return chatbotService.getUserAdInfo("REGION\\65IvanovIS_test");
        //return chatbotService.getQueue();
        //token.init( tokenUnparsedJson );

        //UserAd userAd = userAdInfoClient.getUserAdInfo( "REGION\\65IvanovIS_test" );

        //return userAd.getDepartments();

        /*return token.getToken() + " " +token.getIp() + " " + token.getLogin() + " " + token.getOperatorId() + " " +
        token.getOperatorType() + " " + token.getStart() + " " + token.getEnd() + " " +
        token.getStartTimestamp() + " " + token.getEndTimestamp();*/

    }
}
