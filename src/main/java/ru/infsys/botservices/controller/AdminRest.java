package ru.infsys.botservices.controller;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.infsys.botservices.persistence.entity.Admin;
import ru.infsys.botservices.persistence.entity.ConfigParam;
import ru.infsys.botservices.persistence.entity.Operator;
import ru.infsys.botservices.persistence.repository.AdminRepo;
import ru.infsys.botservices.persistence.repository.ConfigParamRepo;
import ru.infsys.botservices.persistence.repository.OperatorRepo;
import ru.infsys.botservices.service.espp.IntegrationService;
import ru.infsys.botservices.service.espp.model.ConfigParamListInfo;
import ru.infsys.botservices.service.espp.model.OperState;
import ru.infsys.botservices.service.espp.model.OperatorListInfo;

@Controller
@RequestMapping("/admin")
public class AdminRest {

    @Autowired
    OperatorRepo operatorRepo;

    @Autowired
    ConfigParamRepo configParamRepo;

    @Autowired
    IntegrationService integrationService;

    @Autowired
    AdminRepo adminRepo;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @GetMapping("operators")
    public String showCreateForm(Model model) {

        OperatorListInfo operatorsForm = new OperatorListInfo();

        operatorsForm.setOperators(
                operatorRepo.findAll()
                        .stream()
                        .sorted(Comparator.comparing(Operator::getFullName))
                        .collect(Collectors.toList()));

        model.addAttribute("operatorsForm", operatorsForm);

        ConfigParamListInfo configParamsForm = new ConfigParamListInfo();

        configParamsForm.setConfigParams(
                configParamRepo.findAll()
                        .stream()
                        .sorted(Comparator.comparing(ConfigParam::getParamName, String::compareToIgnoreCase))
                        .collect(Collectors.toList()));

        model.addAttribute("configParamForm", configParamsForm);

        return "operatorAdminForm";
    }

    @PostMapping("operators/save")
    public String saveOperators(@ModelAttribute OperatorListInfo operatorsForm, Model model) {

        logger.debug("Изменение навыка операторов начато...");
        List<Operator> operatorList = operatorRepo.findAll();

        Map<Integer, Operator> operatorMap = operatorsForm.getOperators().stream()
                    .collect(Collectors.toMap(Operator::getId, Function.identity()));

        for (Operator operator : operatorList) {

            operator.setChatBotSkill(operatorMap.get(operator.getId()).getChatBotSkill());
            logger.debug("Логин оператора: {}, ChatBotSkill: {}", operator.getLogin(), operator.getChatBotSkill());

            OperState operState = integrationService.getOperStateMapById().get(operator.getId());
            if (operState != null) {
                operState.setChatBotSkill(operator.getChatBotSkill());
            }
        }

        logger.debug("Сохранение операторов начато");
        operatorRepo.saveAll(operatorList);
        logger.debug("Сохранение операторов завершено");

        return "redirect:/admin/operators";
    }


    @PostMapping("parameters/save")
    public String saveParameters(@ModelAttribute ConfigParamListInfo configParamForm, Model model) {

        configParamRepo.saveAll(configParamForm.getConfigParams());

        return "redirect:/admin/operators";
    }

    @GetMapping("")
    public String allAdmins(Model model) {

        List<Admin> admins = adminRepo.findAll();

        model.addAttribute("admins", admins);

        return "admins";

    }

    @GetMapping("/add")
    public String newAdmin(Model model) {

        Admin admin = new Admin();
        model.addAttribute("admin",admin);

        return "addadmin";
    }

    @PostMapping("/add")
    public String saveAdmin(@ModelAttribute Admin admin, Model model) {

        admin.setRoles("ROLE_ADMIN");
        adminRepo.save(admin);

        return "redirect:/admin";
    }

    @GetMapping("/edit/{id}")
    public String editAdmin(@PathVariable int id, Model model) {

        Admin admin = adminRepo.findById(id);

        model.addAttribute("admin",admin);

        return "editadmin";
    }

    @GetMapping("/delete/{id}")
    public String deleteAdmin(@PathVariable int id) {

        adminRepo.deleteById(id);

        return "redirect:/admin";
    }



}
