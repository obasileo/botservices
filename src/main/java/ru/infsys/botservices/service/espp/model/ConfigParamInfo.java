package ru.infsys.botservices.service.espp.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class ConfigParamInfo {

    @ApiModelProperty(
            value = "Наименование конфигурационного параметра",
            example = "dlgMaxCount")
    private String paramName;

    @ApiModelProperty(
            value = "Значение конфигурационного параметра",
            example = "10")
    private String paramValue;

}
