package ru.infsys.botservices.service.espp.model;

import java.util.List;
import lombok.Data;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import ru.infsys.botservices.persistence.entity.Operator;
import ru.infsys.botservices.persistence.repository.OperatorRepo;

@Data
public class OperatorListInfo {

    /*@Autowired
    OperatorRepo operatorRepo;

    @Autowired
    ModelMapper mapper;*/

    //private List<OperatorBaseInfo> operators;
    private List<Operator> operators;

    /*public OperatorListInfo() {

        List<Operator> operatorList = operatorRepo.findAll();
        this.operators = mapper.map(operatorList, new TypeToken<List<OperatorBaseInfo>>() {}.getType());
        //(configParamList, new TypeToken<List<ConfigParamInfo>>() {}.getType());
    }*/
}
