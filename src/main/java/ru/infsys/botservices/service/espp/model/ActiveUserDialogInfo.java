package ru.infsys.botservices.service.espp.model;

import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDateTime;
import lombok.Data;

@Data
public class ActiveUserDialogInfo {

    @ApiModelProperty(value = "Идентификатор пользователя", example = "100")
    private int id;

    @ApiModelProperty(value = "Логин пользователя в формате: «домен\\логин»")
    private String login;

    @ApiModelProperty(value = "ФИО пользователя")
    private String fullName;

    private String operatorLogin;

    private String operatorName;

}
