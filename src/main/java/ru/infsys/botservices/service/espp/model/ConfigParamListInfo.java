package ru.infsys.botservices.service.espp.model;

import java.util.List;
import lombok.Data;
import ru.infsys.botservices.persistence.entity.ConfigParam;

@Data
public class ConfigParamListInfo {

    List<ConfigParam> configParams;
}
