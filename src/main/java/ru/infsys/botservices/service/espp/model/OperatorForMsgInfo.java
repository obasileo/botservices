package ru.infsys.botservices.service.espp.model;

import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.Data;

/**
 * Используется в MessageInfo для формирования информации об операторе.
 */
@Data
public class OperatorForMsgInfo {

    @ApiModelProperty(value = "Идентификатор Оператора")
    private int id;

    @ApiModelProperty(value = "Логин Оператора в формате: «домен\\логин»")
    private String login;

    @ApiModelProperty(value = "ФИО Оператора")
    private String fullName;

    @ApiModelProperty(value = "Должность оператора")
    private String position;

    @ApiModelProperty(value = "Телефоны оператора")
    private String phones;

    @ApiModelProperty(value = "Массив департаментов, в которых состоит оператор")
    private List<String> departments;

    @ApiModelProperty(value = "Строковое представление фото оператора в Base64")
    private String photo;

}
