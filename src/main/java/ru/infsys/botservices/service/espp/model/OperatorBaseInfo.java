package ru.infsys.botservices.service.espp.model;

import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import lombok.Data;

/**
 * Базовые атрибуты Оператора.
 * Используется для возврата атрибутов оператора в сервисе "Получить атрибуты заданного оператора".
 */
@Data
public class OperatorBaseInfo {

    @ApiModelProperty(value = "Идентификатор Оператора")
    private int id;

    @ApiModelProperty(value = "Логин Оператора в формате: «домен\\логин»")
    private String login;

    @ApiModelProperty(value = "ФИО Оператора")
    private String fullName;

    @ApiModelProperty(value = "Должность оператора")
    private String position;

    @ApiModelProperty(value = "Телефоны оператора")
    private String phones;

    @ApiModelProperty(value = "Массив департаментов, в которых состоит оператор")
    private List<String> departments;

    @ApiModelProperty(value = "Строковое представление фото оператора в Base64")
    private String photo;

    @ApiModelProperty(value = "Признак того, что оператору доступен навык Чаи-бот")
    private Boolean chatBotSkill;

    /**
     * Преобразование строкового представления департаментов в список.
     * @param departments - строка департаментов, разделенных запятой.
     */
    public void setDepartments(String departments) {
        if (departments != null) {
            this.departments = Stream.of(departments.split(","))
                    .map(String::trim)
                    .collect(Collectors.toList());
        }
    }
}
