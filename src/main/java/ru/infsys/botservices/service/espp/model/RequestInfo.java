package ru.infsys.botservices.service.espp.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * Запрос СОУ ИТС.
 */
@Data
public class RequestInfo {

    @ApiModelProperty(value = "Идентификатор запроса")
    private String id;

    @ApiModelProperty(value = "Идентификатор диалога пользователя")
    private int userId;

    @ApiModelProperty(value = "Идентификатор первого сообщения, помещенного в запрос")
    private Integer firstMsg;

    @ApiModelProperty(value = "Идентификатор последнего сообщения, помещенного в запрос")
    private Integer lastMsg;

}
