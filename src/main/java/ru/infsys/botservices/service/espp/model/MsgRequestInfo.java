package ru.infsys.botservices.service.espp.model;

import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDateTime;
import lombok.Data;

/**
 * Используется для запроса списка сообщений заданного диалога.
 */
@Data
public class MsgRequestInfo {

    @ApiModelProperty(value = "Идентификатор диалога", example = "101")
    private int id;

    @ApiModelProperty(value = "Максимальное количество выдаваемых сообщений", example = "10")
    private int msgLimit;

    @ApiModelProperty(value = "Метка времени первого сообщения из группы выдаваемых сообщений"
            + "(в формате «yyyy-MM-ddTHH:mm:ss»)", example = "2021-09-25T19:25:11")
    private LocalDateTime firstMsgStamp;

    @ApiModelProperty(value = "Метка времени последнего сообщения из группы выдаваемых сообщений"
            + "(в формате «yyyy-MM-ddTHH:mm:ss»)", example = "2021-09-25T19:25:11")
    private LocalDateTime lastMsgStamp;

}
