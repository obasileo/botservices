package ru.infsys.botservices.service.espp.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDateTime;
import lombok.Data;
import ru.infsys.botservices.persistence.entity.Operator;
import ru.infsys.botservices.service.websocket.model.MessageFile;
import ru.infsys.botservices.service.websocket.model.MessageWithFile;

/**
 * Используется для возврата списка сообщений заданного диалога.
 */
@Data
public class MessageInfo {

    @ApiModelProperty(value = "Идентификатор сообщения")
    private int id;

    @ApiModelProperty(value = "Идентификатор пользователя (диалога)")
    private int userId;

    @ApiModelProperty(value = "Код сообщения: "
            + "0 - сообщение чат-бота пользователю;\n"
            + "1 - сообщение пользователя чат-боту;\n"
            + "2 - сообщение оператора пользователю;\n"
            + "3 - сообщение пользователя оператору;\n"
            + "4 - сообщение пользователю о подключении оператора.")
    private int msgCode;

    @ApiModelProperty(value = "Идентификатор Оператора.")
    private Integer operId;

    @ApiModelProperty(value = "ФИО оператора.")
    private String fullName;

    @ApiModelProperty(value = "Фото оператора.")
    private String photo;

    @ApiModelProperty(value = "Метка времени создания сообщения.")
    private LocalDateTime createStamp;

    @ApiModelProperty(value = "Текст сообщения.")
    private String msg;

    @ApiModelProperty(value = "Метаданные о прикрепленных файлах")
    //List<MessageFile> files;
    MessageWithFile msgFile;

    @JsonIgnore
    Operator operator;

}
