package ru.infsys.botservices.service.espp.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * Сообщение, которое должно быть отправлено пользователю.
 */
@Data
public class MsgToSendInfo {

    @ApiModelProperty(value = "Идентификатор пользователя")
    private int userId;

    @ApiModelProperty(value = "Текст сообщения")
    private String msg;

    public MsgToSendInfo(int userId, String msg) {
        this.userId = userId;
        this.msg = msg;
    }
}
