package ru.infsys.botservices.service.espp.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * Базовые атрибуты Оператора.
 * Используется для возврата атрибутов оператора в сервисе "Получить атрибуты заданного оператора".
 */
@Data
public class OperatorSkill {

    @ApiModelProperty(value = "Идентификатор Оператора")
    private int id;

    @ApiModelProperty(value = "Признак того, что оператору доступен навык Чаи-бот")
    private Boolean chatBotSkill;

}
