package ru.infsys.botservices.service.espp.model;

import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDateTime;
import lombok.Data;

/**
 * Расширенные атрибуты Оператора (включает временя окончания действия токена доступа).
 * Используется для возврата информации об Операторе.
 */
@Data
public class OperatorExtInfo extends OperatorBaseInfo {

    @ApiModelProperty(value = "Метка времени окончания действия токена доступа")
    private LocalDateTime tokenExpiration;

}
