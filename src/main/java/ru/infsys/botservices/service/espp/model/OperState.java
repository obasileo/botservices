package ru.infsys.botservices.service.espp.model;

import java.time.LocalDateTime;
import java.util.UUID;
import lombok.Data;

/**
 * Состояние оператора.
 */
@Data
public class OperState {

    /**
     * Идентификатор оператора
     */
    private int id;

    /**
     * Доступность оператора.
     */
    private boolean available;

    /**
     * Количество активных диалогов, подключенных к оператору.
     */
    private int activeDialogs;

    /**
     * Токен доступа.
     */
    private UUID token;

    /**
     * Метка времени окончания действия токена доступа.
     */
    private LocalDateTime tokenExpiration;

    /**
     * Признак того, что оператору доступен навык Чаи-бот
     */
    private Boolean chatBotSkill;
}
