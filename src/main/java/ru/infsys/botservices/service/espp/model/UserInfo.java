package ru.infsys.botservices.service.espp.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class UserInfo {

    @ApiModelProperty(value = "Идентификатор пользователя")
    int id;

    @ApiModelProperty(value = "Логин пользователя в формате: «домен\\логин»")
    String login;

    @ApiModelProperty(value = "ФИО пользователя")
    String fullName;

    @ApiModelProperty(value = "Должность")
    String position;

    @ApiModelProperty(value = "Телефоны")
    String phones;

    @ApiModelProperty(value = "Список с департаментами, в которых состоит пользователь")
    String departments;

    @ApiModelProperty(value = "Строкове представление фотографии в Base64")
    String photo;

}
