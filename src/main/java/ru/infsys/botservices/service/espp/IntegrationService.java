package ru.infsys.botservices.service.espp;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.stream.Collectors;
import lombok.Getter;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import ru.infsys.botservices.AppConfig;
import ru.infsys.botservices.persistence.entity.ConfigParam;
import ru.infsys.botservices.persistence.entity.Message;
import ru.infsys.botservices.persistence.entity.Operator;
import ru.infsys.botservices.persistence.entity.Request;
import ru.infsys.botservices.persistence.entity.UserDialog;
import ru.infsys.botservices.persistence.repository.ConfigParamRepo;
import ru.infsys.botservices.persistence.repository.MessageRepo;
import ru.infsys.botservices.persistence.repository.OperatorRepo;
import ru.infsys.botservices.persistence.repository.RequestRepo;
import ru.infsys.botservices.persistence.repository.UserDialogRepo;
import ru.infsys.botservices.service.chatbot.ChatbotService;
import ru.infsys.botservices.service.chatbot.model.UserAd;
import ru.infsys.botservices.service.chatbot.model.dialog.ChatBotMessage;
import ru.infsys.botservices.service.chatbot.model.dialog.Description;
import ru.infsys.botservices.service.chatbot.model.dialog.MessageHistory;
import ru.infsys.botservices.service.espp.model.ConfigParamInfo;
import ru.infsys.botservices.service.espp.model.MessageInfo;
import ru.infsys.botservices.service.espp.model.MsgRequestInfo;
import ru.infsys.botservices.service.espp.model.MsgToSendInfo;
import ru.infsys.botservices.service.espp.model.OperState;
import ru.infsys.botservices.service.espp.model.OperatorBaseInfo;
import ru.infsys.botservices.service.espp.model.OperatorExtInfo;
import ru.infsys.botservices.service.espp.model.OperatorSkill;
import ru.infsys.botservices.service.espp.model.RequestInfo;
import ru.infsys.botservices.service.espp.model.UserDialogInfo;
import ru.infsys.botservices.service.websocket.model.MessageWithFile;

/**
 * Сервисы модуля интеграции.
 */
@Service
public class IntegrationService {

    @Autowired
    OperatorRepo operatorRepo;

    @Autowired
    UserDialogRepo userDialogRepo;

    @Autowired
    ConfigParamRepo configparamRepo;

    @Autowired
    MessageRepo messageRepo;

    @Autowired
    RequestRepo requestRepo;

    @Autowired
    ChatbotService chatbotService;

    @Autowired
    AppConfig appConfig;

    @Autowired
    ModelMapper mapper;

    //Сколько сообщений запросить на первой итерации
    private static final int FIRST_MESSAGE_LIMIT = 10;

    //Максимальное количество запрашиваемых сообщений
    private static final int MAX_MESSAGE_LIMIT = 200;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Getter
    private final Map<UUID, OperState> operStateMapByToken = new ConcurrentHashMap<>();

    @Getter
    private final Map<Integer, OperState> operStateMapById = new ConcurrentHashMap<>();

    @Getter
    private final Queue<Integer> connectedUsersQueue = new ConcurrentLinkedQueue<>();

    @Getter
    private final Queue<Integer> disconnectedUsersQueue = new ConcurrentLinkedQueue<>();

    @Getter
    private final Queue<MsgToSendInfo> sendMsgQueue = new ConcurrentLinkedQueue<>();

    /**
     * Вход оператора в систему.
     *
     * @param login - Логин Оператора в формате: «домен\логин»
     * @param token - Токен доступа
     * @return - возвращает информацию об операторе (OperatorInfo).
     */
    public OperatorExtInfo loginOperator(String login, UUID token) throws ResponseStatusException {

        Operator operator = operatorRepo.findByLoginContainingIgnoreCase(login);


        //if (true) {
        if (operator == null) {

            logger.debug("после запроса оператора в базе, operator == null");

            UserAd userAd;

            try {
                userAd = chatbotService.getUserAdInfo(login, true);
                logger.debug("id оператора: {}; логин: {}", userAd.getId(), userAd.getLogin());
            } catch (Exception e) {
                logger.error("Произошла ошибка {} при запросе пользователя ЕСК", e.getMessage());
                throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
            }

            if (userAd.getId() == 0) {
                throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Логин Оператора не найден в ЕСК");
            }

            operator = mapper.map(userAd, Operator.class);
            operator.setFullName(userAd.getSecondName() + " "
                    + userAd.getFirstName() + " "
                    + userAd.getThirdName());
            operator.setLdapSyncStamp(LocalDateTime.now(ZoneId.of("UTC")));

            operator = operatorRepo.save(operator);
            logger.debug("сохранили оператора в базу: {}",
                    operatorRepo.findByLoginContainingIgnoreCase(operator.getLogin()).toString());
        }

        OperState operState;
        int operId = operator.getId();

        if (operStateMapById.containsKey(operId)) {
            operState = operStateMapById.get(operId);
            UUID oldToken = operState.getToken();

            //Если токен просрочен, то в блоке catch выдаем новый токен
            try {
                checkToken(operState.getToken());
                //если токен валидный, то присваиваем текущий токен.
                token = operState.getToken();

            } catch (ResponseStatusException e) {
                operState.setToken(token);
                operState.setTokenExpiration(LocalDateTime.now(ZoneId.of("UTC"))
                        .plusHours(appConfig.getTokenLifeLimit()));
                operStateMapByToken.remove(oldToken);

            }

        } else {
            operState = new OperState();
            operState.setId(operId);
            operState.setAvailable(false);
            operState.setActiveDialogs(0);
            operState.setToken(token);
            operState.setTokenExpiration(LocalDateTime.now(ZoneId.of("UTC"))
                    .plusHours(appConfig.getTokenLifeLimit()));
            operState.setChatBotSkill(operator.getChatBotSkill());

        }

        //Установить текущее время всех диалогов у которых operId = id текущего оператора.
        List<UserDialog> userDialogList = userDialogRepo.findAllByOperId(operState.getId());
        userDialogList.forEach(u -> u.setLastActivityStamp(LocalDateTime.now(ZoneId.of("UTC"))));
        userDialogRepo.saveAll(userDialogList);

        operStateMapByToken.put(token, operState);
        operStateMapById.put(operId, operState);

        OperatorExtInfo operatorExtInfo = mapper.map(operator, OperatorExtInfo.class);
        operatorExtInfo.setTokenExpiration(operState.getTokenExpiration());

        logger.debug("operatorExtInfo: {}", operatorExtInfo);
        return operatorExtInfo;
    }

    /**
     * Выход оператора их системы.
     *
     * @param token - Токен доступа
     */
    public void logoutOperator(UUID token) throws ResponseStatusException {

        OperState operState = checkToken(token);

        if (operState != null) {

            operStateMapById.remove(operState.getId());
            operStateMapByToken.remove(operState.getToken());

            List<UserDialog> userDialogList = userDialogRepo.findAllByOperId(operState.getId());

            for (UserDialog userDialog : userDialogList) {

                userDialog.setLastActivity(0);
                userDialog.setLastActivityStamp(LocalDateTime.now(ZoneId.of("UTC")));
                userDialog.setInQueue(true);
                userDialog.setOperId(null);

                userDialogRepo.save(userDialog);
            }

            //userDialogRepo.moveOperDialogsToQueue(operState.getId());
        }
    }

    /**
     * Проверка токена доступа
     * Если оператор по токену не найден, то возвращаем ошибку - «Оператор не найден»
     * Если токен просрочен (tokenExpiration < текущего времени), то возвращаем ошибку - «Просрочен токен доступа».
     * В случае успешного результата возвращаем объект OperatorInfo.
     *
     * @param token - Токен доступа
     * @return - возвращает объект OperatorInfo
     *
     */
    public OperState checkToken(UUID token) throws ResponseStatusException {

        OperState operState = operStateMapByToken.get(token);

        if (operState == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Оператор не найден");
        }

        if (operState.getTokenExpiration().atZone(ZoneId.of("UTC")).toInstant().toEpochMilli()
                < Instant.now().toEpochMilli()) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Просрочен токен доступа");
        }

        return operState;
    }

    private Operator getOperatorByToken(UUID token) throws ResponseStatusException {

        if (!operStateMapByToken.containsKey(token)) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Оператор не найден");
        }

        return operatorRepo.findById(operStateMapByToken.get(token).getId());

    }

    /**
     * Возвращает список конфигурационных параметров.
     *
     * @param params - Список наименований конфигурационных параметров (наименования отделяются запятыми)
     * @param token - Токен доступа
     *
     * @return - возвращает список конфигурационных параметров.
     */
    public List<ConfigParamInfo> getConfigParams(String params, UUID token) {

        checkToken(token);

        List<String> paramsList = Arrays.stream(params.split(","))
                .map(String::trim).collect(Collectors.toList());

        List<ConfigParam> configParamList = configparamRepo.findByParamNameIn(paramsList);

        return mapper.map(configParamList, new TypeToken<List<ConfigParamInfo>>() {}.getType());

    }

    /**
     * Сохраняет заданные значения конфигурационных параметров в таблицу configparam.
     *
     * @param token - Токен доступа.
     * @param configParamInfoList - Массив конфигурационных параметров.
     */
    public void writeConfigParam(List<ConfigParamInfo> configParamInfoList, UUID token) {

        checkToken(token);

        List<ConfigParam> configParamList
                = mapper.map(configParamInfoList, new TypeToken<List<ConfigParam>>() {}.getType());

        configparamRepo.saveAll(configParamList);
    }

    /**
     * Завершить диалог.
     *
     * @param token - Токен доступа
     * @param userId - Идентификатор пользователя(диалога)
     */
    public synchronized void cancelDialog(UUID token, int userId) {

        OperState operState = checkToken(token);
        UserDialog userDialog = userDialogRepo.findUserDialogById(userId);

        if (userDialog == null || userDialog.getOperId() != operState.getId()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Диалог не связан с данным оператором");
        }

        onCancelUser(userId);
        disconnectedUsersQueue.add(userId);
        logger.debug("add DisconnectedUsersQueue#2: " + userDialog.toString());

        //decreaseActiveDialogs(operState.getId());
        logger.debug("operState#1: " + operState.toString());
    }

    /**
     * Отправить диалог в очередь
     *
     * @param token - Токен доступа
     * @param dialogId - Идентификатор диалога
     */
    public synchronized void dialogToQueue(UUID token, int dialogId) {

        OperState operState = checkToken(token);
        UserDialog userDialog = userDialogRepo.findUserDialogById(dialogId);

        //Проверяем, не находится ли диалог уже в очереди
        if (userDialog != null && userDialog.isInQueue()) {
            return;
        }

        if (userDialog == null || userDialog.getOperId() != operState.getId()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Диалог не связан с данным оператором");
        }

        userDialog.setLastActivity(0);
        userDialog.setLastActivityStamp(LocalDateTime.now(ZoneId.of("UTC")));
        userDialog.setInQueue(true);
        userDialog.setOperId(null);

        userDialogRepo.save(userDialog);

        decreaseActiveDialogs(operState.getId());
        logger.debug("operState#2: " + operState.toString());

    }

    /**
     * Получить список текущих диалогов
     *
     * @param token - Токен доступа
     * @return - возвращает список текущих диалогов
     */
    public List<UserDialogInfo> getDialogList(UUID token) {

        OperState operState = checkToken(token);

        List<UserDialog> userDialogList = userDialogRepo.findCurrentDialogs(operState.getId());

        return mapper.map(userDialogList, new TypeReference<List<UserDialogInfo>>() {}.getType());

    }

    /**
     * Получить список сообщений заданного диалога
     *
     * @param token - Токен доступа.
     * @param msgRequestInfo - Структура входных параметров для запроса сообщений заданного диалога.
     *      id - Идентификатор диалога
     *      msgLimit - Максимальное количество выдаваемых сообщений
     *      firstMsgStamp - Метка времени первого сообщения из группы выдаваемых сообщений
     *      lastMsgStamp - Метка времени последнего сообщения из группы выдаваемых сообщений
     * @return - возвращает список сообщений заданного диалога.
     *      Максимальное количество возвращаемых сообщений задается парамером msgMaxCount таблицы configparam.
     */
    public List<MessageInfo> getMessageList(UUID token, MsgRequestInfo msgRequestInfo) {

        OperState operState = checkToken(token);

        UserDialog userDialog = userDialogRepo.findUserDialogById(msgRequestInfo.getId());

        int msgLimit = msgRequestInfo.getMsgLimit();
        int msgMaxCount = Integer.parseInt(configparamRepo.findByParamName("msgMaxCount").getParamValue());

        LocalDateTime firstMsgStamp = msgRequestInfo.getFirstMsgStamp();
        LocalDateTime lastMsgStamp = msgRequestInfo.getLastMsgStamp();

        if (msgLimit == 0 || msgLimit > msgMaxCount) {
            msgLimit = msgMaxCount;
        }

        List<Message> messageList;

        if (firstMsgStamp == null) {

            messageList = messageRepo
                    .findMessagesByUserId(msgRequestInfo.getId(), PageRequest.of(0, msgLimit));

        } else if (lastMsgStamp == null) {
            messageList = messageRepo
                    .findMessagesByUserIdAndFirstCreateStamp(
                            msgRequestInfo.getId(), firstMsgStamp, PageRequest.of(0, msgLimit));

        } else {
            messageList = messageRepo
                    .findMessagesByUserIdAndBetweenCreateStamp(
                            msgRequestInfo.getId(), firstMsgStamp, lastMsgStamp, PageRequest.of(0, msgLimit));
        }

        List<MessageInfo> messageInfoList = new ArrayList<>();

        for (Message message : messageList) {

            MessageInfo messageInfo = mapper.map(message, MessageInfo.class);
            messageInfo.setMsg(message.getMessage());

            if (message.getMsgFile() != null) {
                try {
                    MessageWithFile messageFileList = new ObjectMapper()
                            .readValue(message.getMsgFile(), new TypeReference<MessageWithFile>() {
                            });
                    messageInfo.setMsgFile(messageFileList);
                } catch (JsonProcessingException e) {
                    logger.error("При десериализации: {} произошла ошибка: {}",
                            message.getMsgFile(), e.getMessage());
                }
            }

            int msgCode = message.getMsgCode();

            switch (msgCode) {
                case 0:
                case 4:
                    messageInfo.setFullName("Чат-бот");
                    break;
                case 1:
                case 3:
                    messageInfo.setFullName(userDialog.getFullName());
                    //messageInfo.setPhoto(userDialog.getPhoto());
                    break;
                case 2:
                    if (message.getOperator() != null) {
                        messageInfo.setOperId(message.getOperator().getId());
                        messageInfo.setFullName(message.getOperator().getFullName());
                        //messageInfo.setPhoto(message.getOperator().getPhoto());
                    }
                    break;
                default: messageInfo.setFullName("");
            }

            messageInfoList.add(messageInfo);
        }

        return messageInfoList;

    }

    /**
     * Отправить сообщение
     *
     * @param token - Токен доступа
     * @param msgToSendInfo - Идентификатор диалога
     */
    public void sendMessage(UUID token, MsgToSendInfo msgToSendInfo) {

        checkToken(token);

        sendMsgQueue.add(msgToSendInfo);

    }

    public void regRequest(UUID token, RequestInfo requestInfo) {

        OperState operState = checkToken(token);

        UserDialog userDialog = userDialogRepo.findUserDialogById(requestInfo.getUserId());

        if (userDialog == null) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "Диалог с userId " + requestInfo.getUserId() + " не найден");
        }

        Request request = requestRepo.findRequestById(requestInfo.getId());

        if (request == null) {
            request = mapper.map(requestInfo, Request.class);
        } else {
            mapper.map(requestInfo, request);
        }

        requestRepo.save(request);

    }

    /**
     * Изменяет статус доступности Оператора.
     *
     * @param token - Токен доступа
     * @param status - Статус доступности (true – доступен, false - занят)
     */
    public void changeOperatorStatus(UUID token, boolean status) {

        OperState operState = checkToken(token);
        operState.setAvailable(status);
    }

    /**
     * Получить атрибуты заданного оператора.
     * @param token - Токен доступа.
     * @param id - Идентификатор Оператора.
     * @return - Возвращает атрибуты заданного оператора.
     */
    public OperatorBaseInfo getOperatorAttr(UUID token, int id) {

        OperState operState = checkToken(token);

        Operator operator = operatorRepo.findById(id);

        if (operator == null) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "Оператор с id " + id + " не найден");
        }

        return mapper.map(operator, OperatorBaseInfo.class);
    }

    /**
     * Получить навык заданного оператора.
     * @param token - Токен доступа.
     * @param id - Идентификатор Оператора.
     * @return - Возвращает навык заданного оператора.
     */
    public OperatorSkill getOperatorSkill(UUID token, int id) {

        OperState operState = checkToken(token);

        return mapper.map(operState, OperatorSkill.class);
    }

    /**
     * Получить список только новых сообщений
     * @param userId - Идентификатор пользователя (диалога).
     * @return - возвращает список сообщений
     */
    public List<Message> getNewMessages(int userId) {

        LocalDateTime maxCreateStampTmp = messageRepo.findMaxCreateStamp(userId);

        if (maxCreateStampTmp == null) {
            maxCreateStampTmp = LocalDateTime.of(2021, 1, 1, 0, 0);
        }

        //Т.к. в лямбда выражениях допускается использование только final либо effectively final переменных,
        // то пришлось дополнительно ввести переменную maxCreateStampTmp.
        LocalDateTime maxCreateStamp = maxCreateStampTmp;

        List<Message> newMessageList = new ArrayList<>();

        int messageCount = FIRST_MESSAGE_LIMIT;

        while (newMessageList.isEmpty() && messageCount < MAX_MESSAGE_LIMIT) {

            try {
                newMessageList = getMessagesFromMessageHistory(
                        chatbotService.getMessageHistory(userId, messageCount, maxCreateStamp))
                        .stream()
                        .filter(m -> m.getCreateStamp().isAfter(maxCreateStamp))
                        .collect(Collectors.toList());
            } catch (Exception e) {
                logger.error("Ошибка получения истории сообщений {} ", e.getStackTrace());
            } finally {
                messageCount = messageCount * 2;
            }
        }

        return newMessageList;
    }

    /**
     * Сформировать список сообщений для модуля интеграции из структуры MessageHistory чат-бота.
     * @param messageHistory - история сообщений чат-бота
     * @return - возвращает список сообщений для модуля интеграции
     */
    public List<Message> getMessagesFromMessageHistory(MessageHistory messageHistory) {

        List<Message> messageList = new ArrayList<>();

        for (ChatBotMessage chatBotMessage : messageHistory.getMessages()) {

            Message message = new Message();
            message.setUserId(chatBotMessage.getUserId());
            message.setMsgCode(chatBotMessage.getMessageCode());
            message.setCreateStamp(chatBotMessage.getCreateDate());
            try {
                message.setMessage(new ObjectMapper()
                        .readValue(chatBotMessage.getDescription(), Description.class)
                        .getMsg());
                messageList.add(message);
            } catch (JsonProcessingException e) {
                logger.error("При десериализации: {} произошла ошибка: {}",
                        chatBotMessage.getDescription(), e.getMessage());
            }
        }
        return messageList;
    }


    /**
     * Перевести диалог в другую очередь (ДКП <-> ЕСПП).
     *
     * @param token - Токен доступа
     * @param transferTo - очередь в которую перевести диалог. Допустимые значения dkp, espp.
     * @param userId - Идентификатор пользователя (диалога)
     */
    public void changeQueue(UUID token, String transferTo, int userId) throws ResponseStatusException {

        OperState operState = checkToken(token);


        if (operState != null) {
            try {
                chatbotService.changeQueue(transferTo, userId);
                onCancelUser(userId);
            } catch (Exception e) {
                throw new ResponseStatusException(
                        HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
            }
        }
    }

    /**
     * Увеличить количество активных диалогов оператора на 1.
     * @param operId - идентификатор оператора
     */
    public synchronized void increaseActiveDialogs(int operId) {

        OperState operState = operStateMapById.get(operId);
        operState.setActiveDialogs(operState.getActiveDialogs() + 1);
        logger.debug("increaseActiveDialogs: " + operState.toString());
    }

    /**
     * Уменьшить количество активных диалогов оператора на 1.
     * @param operId - идентификатор оператора
     */
    public synchronized void decreaseActiveDialogs(int operId) {

        OperState operState = operStateMapById.get(operId);
        operState.setActiveDialogs(operState.getActiveDialogs() - 1);
        logger.debug("decreaseActiveDialogs: " + operState.toString());
    }

    /**
     * При отключении диалога уменьшается количество диалогов оператора на 1
     * и устанавливаются значения соответствующих атрибутов
     * operId = null, userOn = false, inQueue = false
     *
     * @param userId - идентификатор пользователя (диалога)
     */
    public void onCancelUser(int userId) {

        UserDialog userDialog = userDialogRepo.findUserDialogById(userId);
        if (userDialog.getOperId() != null) {
            decreaseActiveDialogs(userDialog.getOperId());
        }
        userDialogRepo.cancelUser(userId);
    }
}
