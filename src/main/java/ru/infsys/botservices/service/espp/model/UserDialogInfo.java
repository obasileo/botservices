package ru.infsys.botservices.service.espp.model;

import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDateTime;
import lombok.Data;

@Data
public class UserDialogInfo {

    @ApiModelProperty(value = "Идентификатор пользователя", example = "100")
    private int id;

    @ApiModelProperty(value = "Логин пользователя в формате: «домен\\логин»")
    private String login;

    @ApiModelProperty(value = "ФИО пользователя")
    private String fullName;

    @ApiModelProperty(value = "Должность")
    private String position;

    @ApiModelProperty(value = "Телефоны")
    private String phones;

    @ApiModelProperty(value = "Список департаментов, в которых состоит пользователь")
    private String departments;

    @ApiModelProperty(value = "Строковое представление фотографии в Base64")
    private String photo;

    @ApiModelProperty(value = "Последняя активность в диалоге "
            + "(0 – подключение, 1 - входящее сообщение, 2 - исходящее сообщение)", example = "2")
    private int lastActivity;

    @ApiModelProperty(value = "Метка времени последней активности в диалоге", example = "2021-09-02T16:26:20")
    private LocalDateTime lastActivityStamp;

    @ApiModelProperty(value = "Признак нахождения в очереди", example = "true")
    private boolean inQueue;

    @ApiModelProperty(value = "Метка времени последнего сообщения в диалоге", example = "2021-09-02T16:26:20")
    private LocalDateTime lastMsgStamp;

}
