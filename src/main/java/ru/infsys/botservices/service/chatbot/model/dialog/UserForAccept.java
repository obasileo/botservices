package ru.infsys.botservices.service.chatbot.model.dialog;

import lombok.Data;

/**
 * Используется для подключения к пользователю,
 * для возврата подключенного пользователя в очередь.
 */
@Data
public class UserForAccept {

    /**
     * Идентификатор пользователя. Используется для подключения к пользователю.
     */
    private int id;

    /**
     * Идентификатор пользователя. Используется для возврата подключенного пользователя в очередь.
     */
    private int userId;

}
