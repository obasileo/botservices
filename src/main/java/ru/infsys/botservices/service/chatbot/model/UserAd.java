package ru.infsys.botservices.service.chatbot.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.util.List;
import lombok.Data;

/**
 * Пользователь Active Directory.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
public class UserAd {

    /**
     * Идентификатор пользователя (или оператора) в системе (информация из Базы Данных системы).
     */
    private int id;

    /**
     * Логин пользователя или оператора.
     */
    private String login;

    /**
     * Имя пользователя или оператора (информация из Active Directory).
     */
    private String firstName;

    /**
     * Фамилия пользователя или оператора (информация из Active Directory).
     */
    private String secondName;

    /**
     * Отчество пользователя или оператора (информация из Active Directory).
     */
    private String thirdName;

    /**
     * Список с департаментами, в которых он состоит (сверху внизу по иерархии из Active Directory).
     */
    private List<String> departments;

    /**
     * Список с группами Active Directory, в которых он состоит.
     */
    private List<String> memberOf;

    /**
     * Строкове представление фотографии в Base64 (информация из Active Directory).
     */
    private String photo;

    /**
     * Телефоны (информация из Active Directory).
     */
    private String phones;

    /**
     * Должность (информация из Active Directory).
     */
    private String position;


    /**
     * Входной параметр для получения информации о пользователе.
     * Логин пользователя или оператора.
     */
    private String userLogin;

    /**
     * Входной параметр для получения информации о пользователе.
     * Принадлежность к группе операторов.
     */
    private Boolean isOperator;
}


