package ru.infsys.botservices.service.chatbot;

import java.time.LocalDateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import ru.infsys.botservices.AppConfig;
import ru.infsys.botservices.service.chatbot.model.UserAd;
import ru.infsys.botservices.service.chatbot.model.dialog.DataForAccept;
import ru.infsys.botservices.service.chatbot.model.dialog.DataForMessageHistory;
import ru.infsys.botservices.service.chatbot.model.dialog.MessageHistory;
import ru.infsys.botservices.service.chatbot.model.dialog.MessageToUser;
import ru.infsys.botservices.service.chatbot.model.dialog.UserForAccept;

/**
 * Сервис взаимодействия с ПО «Чат-бот».
 */
@Service
public class ChatbotService20211117 {

    @Autowired
    Auth auth;

    @Autowired
    AppConfig appConfig;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private HttpHeaders prepareRequestHeaders(String key) throws Exception {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        try {
            headers.set(key, auth.getToken());
        } catch (Exception e) {
            logger.error("Произошла ошибка {} при запросе токена", e.getMessage());
            throw e;
        }
        return headers;
    }

    /**
     * Получить информацию о пользователе (операторе) по его логину.
     * @param userLogin - логин пользователя в формате 'REGION\65IvanovIS' о котором необходимо получить инфо.
     * @return - возвращает информацию о пользователе Active Directory.
     */
    public UserAd getUserAdInfo(String userLogin) throws Exception {

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = prepareRequestHeaders("token");

        UserAd userAd = new UserAd();
        userAd.setUserLogin(userLogin);
        userAd.setIsOperator(true);

        HttpEntity<UserAd> entity = new HttpEntity<>(userAd, headers);

        ResponseEntity<UserAd> response;

        try {
            response = restTemplate.exchange(appConfig.getOperatorURL() + "/user-ad-info",
                    HttpMethod.POST,
                    entity,
                    UserAd.class);
        } catch (Exception e) {
            logger.error("Ошибка при вызове REST: {}", e.getMessage());
            throw e;
        }

        if (response.getStatusCode() == HttpStatus.OK) {
            return response.getBody();

        } else {
            logger.error("Код ответа: {} Ошибка: {}", response.getStatusCode(), response.getBody());
            throw new Exception(response.getBody().toString());
        }
    }

    /**
     * Подключение к пользователю и получение истории сообщений по нему.
     * @param userId - Идентификатор пользователя
     * @param messageLimit - максимальное количество возвращаемых сообщений
     * @return - возвращает статус подключения и историю сообщений
     */
    public MessageHistory acceptUser(int userId, int messageLimit) throws Exception {

        HttpHeaders headers = prepareRequestHeaders("token");

        DataForAccept dataForAccept = new DataForAccept();
        dataForAccept.getUser().setId(userId);
        dataForAccept.setMessageLimit(messageLimit);

        HttpEntity<DataForAccept> entity = new HttpEntity<>(dataForAccept, headers);

        ResponseEntity<MessageHistory> response;

        try {
            response = new RestTemplate().exchange(appConfig.getOperatorURL() + "/accept",
                    HttpMethod.POST,
                    entity,
                    MessageHistory.class);
        } catch (Exception e) {
            logger.error("Ошибка при вызове REST: {}", e.getMessage());
            throw e;
        }

        if (response.getStatusCode() == HttpStatus.OK) {
            return response.getBody();
        } else {
            logger.error("Код ответа: {} Ошибка: {}", response.getStatusCode(), response.getBody());
            throw new Exception(response.getBody().toString());
        }
    }

    /**
     * Отключение от текущего пользователя.
     * @param userId - Идентификатор подключенного к оператору пользователя
     * @return - при успешном отключении от текущего пользователя возвращает строку "disconnect done"
     */
    public String cancelUser(int userId) throws Exception {

        HttpHeaders headers = prepareRequestHeaders("token");

        UserForAccept userForAccept = new UserForAccept();
        userForAccept.setUserId(userId);

        HttpEntity<UserForAccept> entity = new HttpEntity<>(userForAccept, headers);

        ResponseEntity<String> response;

        try {
            response = new RestTemplate().exchange(appConfig.getOperatorURL() + "/cancel",
                    HttpMethod.POST,
                    entity,
                    String.class);
        } catch (Exception e) {
            logger.error("Ошибка при вызове REST: {}", e.getMessage());
            throw e;
        }

        if (response.getStatusCode() == HttpStatus.OK) {
            return response.getBody();
        } else {
            logger.error("Код ответа: {} Ошибка: {}", response.getStatusCode(), response.getBody());
            throw new Exception(response.getBody());
        }
    }

    /**
     * Получить историю сообщений пользователя.
     * @param userId - Идентификатор пользователя
     * @messageLimit - максимальное количество возвращаемых сообщений
     * @messageDateFrom - дата, начиная с которой получить сообщения
     * @return - возвращает историю сообщений
     */
    public MessageHistory getMessageHistory(int userId, int messageLimit, LocalDateTime messageDateFrom)
            throws Exception {

        HttpHeaders headers = prepareRequestHeaders("token");

        DataForMessageHistory dataForMessageHistory = new DataForMessageHistory();
        dataForMessageHistory.setUserId(userId);
        dataForMessageHistory.setMessageLimit(messageLimit);
        dataForMessageHistory.setMessageDateFrom(messageDateFrom);

        HttpEntity<DataForMessageHistory> entity = new HttpEntity<>(dataForMessageHistory, headers);

        ResponseEntity<MessageHistory> response;

        try {
            response = new RestTemplate().exchange(appConfig.getOperatorURL() + "/history-users",
                    HttpMethod.POST,
                    entity,
                    MessageHistory.class);
        } catch (Exception e) {
            logger.error("Ошибка при вызове REST: {}", e.getMessage());
            throw e;
        }

        if (response.getStatusCode() == HttpStatus.OK) {
            return response.getBody();
        } else {
            logger.error("Код ответа: {} Ошибка: {}", response.getStatusCode(), response.getBody());
            throw new Exception(response.getBody().toString());
        }
    }

    /**
     * Отправить сообщение пользователю.
     * @param userId - Идентификатор подключенного к оператору пользователя
     * @param message - Сообщение пользователю
     * @return - при успешной отправке сообщения возвращает строку "message sent"
     */
    public String sendMessageToUser(int userId, String message) throws Exception {

        HttpHeaders headers = prepareRequestHeaders("token");

        MessageToUser messageToUser = new MessageToUser();
        messageToUser.setUserId(userId);
        messageToUser.setMessage(message);

        HttpEntity<MessageToUser> entity = new HttpEntity<>(messageToUser, headers);

        ResponseEntity<String> response = null;

        try {
            response = new RestTemplate().exchange(appConfig.getOperatorURL() + "/send",
                    HttpMethod.POST,
                    entity,
                    String.class);
        } catch (Exception e) {
            logger.error("Ошибка при вызове REST: {}", e.getMessage());
            throw e;
        }

        if (response.getStatusCode() == HttpStatus.OK) {
            return response.getBody();
        } else {
            logger.error("Код ответа: {} Ошибка: {}", response.getStatusCode(), response.getBody());
            throw new Exception(response.getBody().toString());
        }
    }
}
