package ru.infsys.botservices.service.chatbot;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.text.ParseException;
import java.time.Instant;
import javax.annotation.PostConstruct;
import lombok.Getter;
import org.apache.tomcat.util.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import ru.infsys.botservices.AppConfig;
import ru.infsys.botservices.service.chatbot.model.DataForAuth;
import ru.infsys.botservices.service.chatbot.model.DataForEncrypt;
import ru.infsys.botservices.service.chatbot.model.TokenData;
import ru.infsys.botservices.utils.Crypto;
import ru.infsys.botservices.utils.DateTimeConverter;

/**
 * Класс для авторизации в ПО Чат-бот.
 * Все сервисы API чат-бот для авторизации используют токен доступа,
 * который передается в заголовке Authorization.
 * Здесь реализованы методы для получения токена доступа и инициализации полей объекта TokenData
 * для последующей работы с API чат-бот.
 */
@Component
public class Auth {

    @Autowired
    AppConfig appConfig;

    @Autowired
    RestTemplate restTemplate;

    @Getter
    private TokenData tokenData;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * Получает токен доступа и инициализирует поля в объекте Token.
     */
    public void tokenInit() throws Exception {

        //RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<DataForAuth> entity = new HttpEntity<>(
                new DataForAuth(
                        new Crypto().encryption(appConfig.getEncryptionPsw(), getJsonForEncrypt()),
                        appConfig.getSystemName()),
                headers);

        ResponseEntity<TokenData> response;

        try {
            response = restTemplate.exchange(appConfig.getOperatorURL() + "/auth-system",
                    HttpMethod.POST,
                    entity,
                    TokenData.class);
        } catch (Exception e) {
            logger.error("Ошибка при вызове REST: {}", e.getMessage());
            throw e;
        }

        if (response.getStatusCode() == HttpStatus.OK) {
            init(response.getBody().getToken());
        } else {
            logger.error("Код ответа: {} Ошибка: {}", response.getStatusCode(), response.getBody().getError());
            throw new Exception(response.getBody().getError());
        }
    }

    private String getJsonForEncrypt() throws JsonProcessingException {

        DataForEncrypt dataForEncrypt = new DataForEncrypt();

        dataForEncrypt.setSystem(appConfig.getSystemName());
        dataForEncrypt.setUser(appConfig.getAccount());
        dataForEncrypt.setTime(DateTimeConverter.getUtcDateTimeString());

        try {
            return new ObjectMapper().writeValueAsString(dataForEncrypt);
        } catch (JsonProcessingException e) {
            logger.error("При сериализации: {} произошла ошибка: {}", dataForEncrypt, e.getMessage());
            throw e;
        }
    }

    private void init(String token) throws ParseException, JsonProcessingException {

        try {
            tokenData = new ObjectMapper().readValue(new String(Base64.decodeBase64(
                    token.substring(0, token.indexOf(".")))), TokenData.class);
        } catch (JsonProcessingException e) {
            logger.error("При десериализации: {} произошла ошибка: {}", token, e.getMessage());
            throw e;
        }

        tokenData.setToken(token);
        tokenData.setStartTimestamp(convertDateTime(tokenData.getStart()));
        tokenData.setEndTimestamp(convertDateTime(tokenData.getEnd()));

    }

    private long convertDateTime(String dateTime) throws ParseException {
        try {
            return DateTimeConverter.getUtcDateTimeFromString(dateTime);
        } catch (ParseException e) {
            logger.error("Ошибка преобразования {} в Timestamp {} ", dateTime, e.getMessage());
            throw e;
        }
    }

    /**
     * Метод возвращает актуальный токен доступа для работы с API чат-бот.
     * В случае, если токен доступа не инициализирован или срок действия его истек,
     * то через сервис ПО чат-бот будет запрошен новый токен.
     *
     * @return - возвращает токен доступа.
     */
    public String getToken() throws Exception {
        if (tokenData == null || tokenData.getToken() == null
                || Instant.now().toEpochMilli() > tokenData.getEndTimestamp()) {
            try {
                tokenInit();
                logger.info("Токен успешно инициализирован. token: {}", tokenData.getToken());
            } catch (Exception e) {
                logger.error("Ошибка инициализации токена", e);
                throw e;
            }
        }
        return tokenData.getToken();
    }

    /**
     * Метод используется для получения токена доступа и
     * инициализации полей объекта TokenData при старте приложения.
     */
    @PostConstruct
    private void afterPropertiesSet() {
        try {
            tokenInit();
            logger.info("Токен успешно инициализирован. token: {}", tokenData.getToken());
        } catch (Exception e) {
            logger.error("Ошибка инициализации токена", e);
        }
    }
}
