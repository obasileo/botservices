package ru.infsys.botservices.service.chatbot.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.NonNull;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
public class DataForAuth {

    /**
     * Зашифрованные алгоритмом AES данные в виде строки.
     * Данные для шифрования берутся в виде json из DataForEncrypt.
     */
    @NonNull
    private String data;

    /**
     * Название сторонней системы (предоставляется разработчиками API чат-бот).
     */
    @NonNull
    private String system;
}
