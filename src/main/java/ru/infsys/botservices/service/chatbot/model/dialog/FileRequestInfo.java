package ru.infsys.botservices.service.chatbot.model.dialog;

import lombok.Data;

@Data
public class FileRequestInfo {

    private int fileId; //":364,
    private String fileName; //":"yyy.xml",
    private String updateDate; //":"18dc1a8521842d3d957be38d48b50376854f5b770e9b81547394755f7ab22ea481dc47e52d4e53ae
    // 64ac549a54c6be95ba14ed9d7d94118dc459bfaaaa175271",
    private int sourceType; //":3

}
