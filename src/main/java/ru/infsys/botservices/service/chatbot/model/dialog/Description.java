package ru.infsys.botservices.service.chatbot.model.dialog;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * Поле description из ChatBotMessage.
 * В ChatBotMessage данное поле имеет тип String, т.к. от чат-бот возвращается именно строка, а не объект.
 * При этом строка имеет структуру объекта.
 * Уточнял у разработчиков, они подтвердили, что возвращают объект в виде строки.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class Description {

    private String msg;
    private String type;

    //TODO Поменялась структура данного атрибута, поэтому закомментировал.
    // В принципе для нас он и не нужен. Нужно будет удалить, если все нормально будет работать.
    //private String payload;

    @JsonProperty("user_id")
    private int userId;
    private String last;
    private String state;

    @JsonProperty("switch_to_operator")
    private String switchToOperator;

    @JsonProperty("message_code")
    private int messageCode;
}
