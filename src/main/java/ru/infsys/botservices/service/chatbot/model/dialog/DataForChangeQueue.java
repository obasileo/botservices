package ru.infsys.botservices.service.chatbot.model.dialog;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Структура для перевода диалога в другую очередь (ДКП <-> ЕСПП).
 */
@Data
@AllArgsConstructor
public class DataForChangeQueue {

    @ApiModelProperty(value = "Название очереди. Допустимые значения dkp, espp")
    private String transferTo;

    @ApiModelProperty(value = "Идентификатор пользователя (диалога)")
    private int userId;

}
