package ru.infsys.botservices.service.chatbot;

import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import ru.infsys.botservices.AppConfig;
import ru.infsys.botservices.service.chatbot.model.dialog.DataForAccept;
import ru.infsys.botservices.service.chatbot.model.dialog.MessageHistory;
import ru.infsys.botservices.service.chatbot.model.dialog.MessageToUser;
import ru.infsys.botservices.service.chatbot.model.dialog.UserForAccept;
import ru.infsys.botservices.service.websocket.model.UserInQueue;

@Service
public class DialogService {

    @Autowired
    Auth auth;

    @Autowired
    AppConfig appConfig;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private HttpHeaders prepareRequestHeaders(String key) throws Exception {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        try {
            headers.set(key, auth.getToken());
        } catch (Exception e) {
            logger.error("Произошла ошибка {} при запросе токена", e.getMessage());
            throw e;
        }
        return headers;
    }

    /**
     * Подключение к пользователю и получение истории сообщений по нему.
     *
     * @param userInQueueList - List очереди пользователей
     * @param messageLimit - Сколько сообщений из истории послать
     * @return - возвращает статус подключения и историю сообщений
     */
    public void acceptUser(List<UserInQueue> userInQueueList, int messageLimit) throws Exception {

        HttpHeaders headers = prepareRequestHeaders("token");

        DataForAccept dataForAccept = new DataForAccept();
        for (UserInQueue userInQueue : userInQueueList) {
            dataForAccept.getUser().setId(userInQueue.getId());
            dataForAccept.setMessageLimit(messageLimit);

            /*dataForAccept.getUser().setId(userId);
            dataForAccept.setMessageLimit(messageLimit);*/
            /*User user = new User();
            user.setId(userId);*/

            HttpEntity<DataForAccept> entity = new HttpEntity<>(dataForAccept, headers);
            //HttpEntity<User> entity = new HttpEntity<>(user, headers);

            ResponseEntity<MessageHistory> response = null;

            try {
                response = new RestTemplate().exchange(appConfig.getOperatorURL() + "/accept",
                        HttpMethod.POST,
                        entity,
                        MessageHistory.class);
            } catch (Exception e) {
                logger.error("Ошибка при вызове REST: {}", e.getMessage());
                throw e;
            }

            if (response.getStatusCode() == HttpStatus.OK) {
                //return response.getBody();
            } else {
                logger.error("Код ответа: {} Ошибка: {}", response.getStatusCode(), response.getBody());
                throw new Exception(response.getBody().toString());
            }
        }
    }

    /**
     * Возвращает подключенного пользователя в очередь.
     *
     * @param userId - Идентификатор подключенного пользователя
     * @return - при успешном возврате пользователя в очередь возвращает строку "return done"
     */
    public String returnUser(int userId) throws Exception {

        HttpHeaders headers = prepareRequestHeaders("token");

        UserForAccept userForAccept = new UserForAccept();
        userForAccept.setUserId(userId);

        HttpEntity<UserForAccept> entity = new HttpEntity<>(userForAccept, headers);

        ResponseEntity<String> response = null;

        try {
            response = new RestTemplate().exchange(appConfig.getOperatorURL() + "/return",
                    HttpMethod.POST,
                    entity,
                    String.class);
        } catch (Exception e) {
            logger.error("Ошибка при вызове REST: {}", e.getMessage());
            throw e;
        }

        if (response.getStatusCode() == HttpStatus.OK) {
            return response.getBody();
        } else {
            logger.error("Код ответа: {} Ошибка: {}", response.getStatusCode(), response.getBody());
            throw new Exception(response.getBody().toString());
        }
    }

    /**
     * Отключение от текущего пользователя.
     *
     * @param userId - Идентификатор подключенного к оператору пользователя
     * @return - при успешном отключении от текущего пользователя возвращает строку "disconnect done"
     */
    public String cancelUser(int userId) throws Exception {

        HttpHeaders headers = prepareRequestHeaders("token");

        UserForAccept userForAccept = new UserForAccept();
        userForAccept.setUserId(userId);

        HttpEntity<UserForAccept> entity = new HttpEntity<>(userForAccept, headers);

        ResponseEntity<String> response = null;

        try {
            response = new RestTemplate().exchange(appConfig.getOperatorURL() + "/cancel",
                    HttpMethod.POST,
                    entity,
                    String.class);
        } catch (Exception e) {
            logger.error("Ошибка при вызове REST: {}", e.getMessage());
            throw e;
        }

        if (response.getStatusCode() == HttpStatus.OK) {
            return response.getBody();
        } else {
            logger.error("Код ответа: {} Ошибка: {}", response.getStatusCode(), response.getBody());
            throw new Exception(response.getBody().toString());
        }
    }

    /**
     * Отправка сообщения пользователю.
     *
     * @param userId - Идентификатор подключенного к оператору пользователя
     * @param message - Сообщение пользователю
     * @return - при успешной отправке сообщения пользователю возвращает строку "message sent"
     */
    public String sendMessageToUser(int userId, String message) throws Exception {

        HttpHeaders headers = prepareRequestHeaders("token");

        MessageToUser messageToUser = new MessageToUser();
        messageToUser.setUserId(userId);
        messageToUser.setMessage(message);

        HttpEntity<MessageToUser> entity = new HttpEntity<>(messageToUser, headers);

        ResponseEntity<String> response = null;

        try {
            response = new RestTemplate().exchange(appConfig.getOperatorURL() + "/send",
                    HttpMethod.POST,
                    entity,
                    String.class);
        } catch (Exception e) {
            logger.error("Ошибка при вызове REST: {}", e.getMessage());
            throw e;
        }

        if (response.getStatusCode() == HttpStatus.OK) {
            return response.getBody();
        } else {
            logger.error("Код ответа: {} Ошибка: {}", response.getStatusCode(), response.getBody());
            throw new Exception(response.getBody().toString());
        }
    }
}
