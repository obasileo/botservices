package ru.infsys.botservices.service.chatbot.model.dialog;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.util.List;
import lombok.Data;

/**
 * Структура, возвращаемая при запросе истории сообщений в чат-бот (/history-users)
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
public class MessageHistory {

    private boolean success;

    private List<ChatBotMessage> messages;

    private boolean isMore;

}
