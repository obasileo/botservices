package ru.infsys.botservices.service.chatbot.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

/**
 * Класс TokenData содержит:
 * 1. Непосредственно сам токен доступа.
 * Токен состоит из 2-х частей, разделенных точкой: <строка в BASE64>.<зашифрованная часть>.
 * Первую часть в BASE64 можно расшифровать и использовать данные для своих целей.
 * Вторая часть зашифрована только для сервера.
 * 2. Поля, полученные из первой части токена.
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
public class TokenData {

    /**
     * Токен доступа для последующей работы с API чат-бот.
     */
    private String token;

    /**
     * IP адрес, с которого пришел запрос.
     */
    private String ip;

    /**
     * Логин оператора в домене.
     */
    private String login;

    /**
     * Идентификатор оператора в системе.
     */
    private int operatorId;

    /**
     * Тип оператора в текстовом виде на английском. Допустимые значения: dkp, espp.
     */
    private String operatorType;

    /**
     * Статус выдачи токена (всегда 0). Допустимые значения: 0.
     */
    private int status;

    /**
     * Дата и время начала действия токена (UTC).
     */
    private String start;

    /**
     * Дата и время окончания действия токена (UTC).
     */
    private String end;

    /**
     * Дата и время начала действия токена (UTC) в миллисекундах.
     */
    private long startTimestamp;

    /**
     * Дата и время окончания действия токена (UTC) в миллисекундах.
     */
    private long endTimestamp;


    /**
     * Описание возможной ошибки при получении токена.
     */
    private String error;

}
