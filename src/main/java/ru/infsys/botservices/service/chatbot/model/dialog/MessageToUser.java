package ru.infsys.botservices.service.chatbot.model.dialog;

import lombok.Data;

@Data
public class MessageToUser {

    /**
     * Id подключенного к оператору пользователя.
     */
    private int userId;

    /**
     * Сообщение пользователю.
     */
    private String message;

}
