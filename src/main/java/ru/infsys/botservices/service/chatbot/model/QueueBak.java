package ru.infsys.botservices.service.chatbot.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

/**
 * Очередь пользователей.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
public class QueueBak {

    /**
     * Идентификатор пользователя в системе
     */
    private int id;

    /**
     * ФИО пользователя
     */
    private String name;

    /**
     * Логин пользователя в домене
     */
    private String login;

    /**
     * Для какой очереди пользователь: espp или dkp. Допустимые значения: "espp", "dkp"
     */
    private String operatorType;

}

