package ru.infsys.botservices.service.chatbot.model.dialog;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.time.LocalDateTime;
import lombok.Data;

/**
 * Структура для запроса истории сообщений в чат-бот (/history-users)
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
public class DataForMessageHistory {

    /**
     * Идентификатор пользователя
     */
    private int userId;

    /**
     * Максимальное количество возвращаемых сообщений
     */
    private int messageLimit;

    /**
     * Дата, начиная с которой получить сообщения
     */
    private LocalDateTime messageDateFrom;

}
