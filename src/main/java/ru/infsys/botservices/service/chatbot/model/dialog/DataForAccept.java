package ru.infsys.botservices.service.chatbot.model.dialog;

import lombok.Data;

/**
 * Данные для подключения к пользователю и получения истории сообщений по нему.
 */
@Data
public class DataForAccept {

    public DataForAccept() {

        this.user = new UserForAccept();
    }

    private UserForAccept user;

    /**
     * Максимальное количество возвращаемых сообщений (необязательный, по умолчанию 10)
     */
    private int messageLimit;

}
