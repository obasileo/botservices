package ru.infsys.botservices.service.chatbot;

import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import ru.infsys.botservices.AppConfig;
import ru.infsys.botservices.service.chatbot.model.QueueBak;
import ru.infsys.botservices.service.chatbot.model.UserAd;

/**
 * Сервис взаимодействия с ПО «Чат-бот» .
 */
@Service
public class ChatbotServiceBkp {

    @Autowired
    Auth auth;

    @Autowired
    AppConfig appConfig;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private HttpHeaders prepareRequestHeaders(String key) throws Exception {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        try {
            headers.set(key, auth.getToken());
        } catch (Exception e) {
            logger.error("Произошла ошибка {} при запросе токена", e.getMessage());
            throw e;
        }
        return headers;
    }

    /**
     * Получить информацию о пользователе (операторе) по его логину.
     *
     * @param userLogin - логин пользователя в формате 'REGION\65IvanovIS' о котором необходимо получить инфо.
     * @return - возвращает информацию о пользователе Active Directory.
     */
    public UserAd getUserAdInfo(String userLogin) throws Exception {

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = prepareRequestHeaders("token");

        UserAd userAd = new UserAd();
        userAd.setUserLogin(userLogin);
        userAd.setIsOperator(true);

        HttpEntity<UserAd> entity = new HttpEntity<>(userAd, headers);

        ResponseEntity<UserAd> response = null;

        try {
            response = restTemplate.exchange(appConfig.getOperatorURL() + "/user-ad-info",
                    HttpMethod.POST,
                    entity,
                    UserAd.class);
        } catch (Exception e) {
            logger.error("Ошибка при вызове REST: {}", e.getMessage());
            throw e;
        }

        if (response.getStatusCode() == HttpStatus.OK) {
            return response.getBody();
        } else {
            logger.error("Код ответа: {} Ошибка: {}", response.getStatusCode(), response.getBody());
            throw new Exception(response.getBody().toString());
        }
    }

    public List<QueueBak> getQueue() throws Exception {

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = prepareRequestHeaders("Authorization");

        ResponseEntity<List<QueueBak>> response = null;

        try {
            response = restTemplate.exchange(appConfig.getOperatorURL() + "/queue",
                    HttpMethod.POST,
                    new HttpEntity<>(headers),
                    new ParameterizedTypeReference<List<QueueBak>>() {
                    });
        } catch (Exception e) {
            logger.error("Ошибка при вызове REST: {}", e.getMessage());
            throw e;
        }

        if (response.getStatusCode() == HttpStatus.OK) {
            return response.getBody();
        } else {
            logger.error("Код ответа: {} Ошибка: {}", response.getStatusCode(), response.getBody());
            throw new Exception(response.getBody().toString());
        }
    }

}
