package ru.infsys.botservices.service.chatbot.model.dialog;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.time.LocalDateTime;
import lombok.Data;

/**
 * Структура сообщения в чат-бот.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
public class ChatBotMessage {

    private String id;

    @JsonProperty("message_code")
    private int messageCode;

    private String description;

    @JsonProperty("operator_id")
    private int operatorId;

    @JsonProperty("pattern_id")
    private int patternId;

    @JsonProperty("create_date")
    private LocalDateTime createDate;

    @JsonProperty("answer_type_id")
    private int answerTypeId;

    @JsonProperty("user_id")
    private int userId;

    @JsonProperty("operator_type")
    private int operatorType;

    @JsonProperty("feedback_type")
    private String feedbackType;

    @JsonProperty("user_fio")
    private String userFio;

    @JsonProperty("user_login")
    private String userLogin;

}

