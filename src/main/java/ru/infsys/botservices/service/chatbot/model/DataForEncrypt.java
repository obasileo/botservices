package ru.infsys.botservices.service.chatbot.model;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

/**
 * Объект для шифрования.
 * При работе с API чат-бот сторонние системы сначала должны авторизоваться,
 * передав данный объект в виде JSON, зашифрованный алгоритмом AES-256-CBC.
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
public class DataForEncrypt {

    /**
     * Название сторонней системы (предоставляется разработчиками API чат-бот).
     */
    private String system;

    /**
     * Наименование учетной записи, от имени которой работает модуль интеграции.
     */
    private String user;

    /**
     * Дата и время (UTC) формирования данного сообщения в формате «yyyy-mm-dd hh:nn:ss».
     */
    private String time;

}
