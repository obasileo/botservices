package ru.infsys.botservices.service.websocket.model;

import lombok.Data;

@Data
public class MessageFile {

    private String fileName; // "1.txt",
    private String updateDate; // "cddbac4624d706e470ab634f0ae0a7882ba19e1b61ebeaed50cc36e34ae9301f67622932a74a6
    // 8b8147d9ee3af8ab716b1bcc05613bd607c2aa0f3796fae045a",
    private Long fileSize; //5,
    private String type; //"file",
    private Long fileId; //382

}
