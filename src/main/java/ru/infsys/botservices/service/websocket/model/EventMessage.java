package ru.infsys.botservices.service.websocket.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

/**
 * Сообщение, полученное через WebSocket в событии messages
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class EventMessage {

    /**
     * Идентификатор пользователя
     */
    private int userId;

    /**
     * Тип сообщения:
     * operator - сообщение отправлено оператором
     * user - сообщение отправлено пользователем
     */
    private String type;

    /**
     * Тело сообщения
     */
    private String message;
}
