package ru.infsys.botservices.service.websocket.model;

import lombok.Data;

@Data
public class EventMessageWithFile {

    /**
     * Идентификатор пользователя
     */
    private int userId;

    /**
     * Тип сообщения:
     * operator - сообщение отправлено оператором
     * user - сообщение отправлено пользователем
     */
    private String type;

    /**
     * Тело сообщения
     */
    private MessageWithFile message;
}
