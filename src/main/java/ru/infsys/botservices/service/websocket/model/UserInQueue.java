package ru.infsys.botservices.service.websocket.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

/**
 * Структура, полученная через WebSocket в событии queue (очередь пользователей)
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserInQueue extends ConnectedUser {

    /**
     * Для какой очереди пользователь: espp или dkp.
     * Допустимые значения: "espp", "dkp".
     */
    private String operatorType;

}
