package ru.infsys.botservices.service.websocket.model;

import java.util.List;
import lombok.Data;

@Data
public class MessageWithFile {

    private Integer sourceType; //3,
    private Integer messageCode; //6,
    private List<MessageFile> files;

}
