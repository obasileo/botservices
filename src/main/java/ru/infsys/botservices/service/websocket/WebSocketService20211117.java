package ru.infsys.botservices.service.websocket;

import static java.util.Collections.singletonMap;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import java.net.URI;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.infsys.botservices.AppConfig;
import ru.infsys.botservices.persistence.entity.Message;
import ru.infsys.botservices.persistence.entity.UserDialog;
import ru.infsys.botservices.persistence.repository.OperatorRepo;
import ru.infsys.botservices.persistence.repository.UserDialogRepo;
import ru.infsys.botservices.service.chatbot.Auth;
import ru.infsys.botservices.service.espp.IntegrationService;
import ru.infsys.botservices.service.websocket.model.Command;
import ru.infsys.botservices.service.websocket.model.ConnectedUser;
import ru.infsys.botservices.service.websocket.model.EventMessage;
import ru.infsys.botservices.service.websocket.model.UserInQueue;


/**
 * Сервисы подключения к socket.io и обработки событий вебсокета.
 */
@Service
public class WebSocketService20211117 {

    @Autowired
    Auth auth;

    @Autowired
    AppConfig appConfig;

    @Autowired
    UserDialogRepo userDialogRepo;

    @Autowired
    OperatorRepo operatorRepo;

    @Autowired
    IntegrationService integrationService;

    @Autowired
    ModelMapper mapper;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * Признак инициализации модуля интеграции. При старте приложения уставлен в false.
     * Необходим для обработки события connectedUsers только при запуске приложения.
     */
    private boolean imInitialized = false;

    private Socket socket;

    /**
     * Подключение к socket.io
     */
    private void socketConnect() {

        try {
            IO.Options options = IO.Options.builder()
                    .setAuth(singletonMap("token", auth.getToken()))
                    .build();
            socket = IO.socket(URI.create(appConfig.getOperatorURL()), options);
            socket.connect();

        } catch (Exception e) {
            logger.error("При получении токена произошла ошибка: {}", e.getStackTrace());
        }
    }

    /**
     * Инициализация соединения с socket.io, подписка на события и их обработка.
     */
    /*@PostConstruct
    public void runSocket() {

        socketConnect();

        socket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                logger.info("Подключено к socket.io");
            }
        });

        socket.on(Socket.EVENT_CONNECT_ERROR, new Emitter.Listener() {
            @Override
            public void call(Object... objects) {
                logger.error("Ошибка подключения к socket.io {}", objects[0].toString());
                socketConnect();
            }
        });

        //Обработка события commands
        socket.on("commands:" + auth.getTokenData().getOperatorId(), new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                logger.info("event commands: " + args[0].toString());

                Command command = null;
                try {
                    command = new ObjectMapper()
                            .readValue(args[0].toString(), Command.class);
                } catch (JsonProcessingException e) {
                    logger.error("При десериализации: {} произошла ошибка: {}", args[0], e.getMessage());
                }

                if (command.getType().equals("accept") || command.getType().equals("cancel")) {

                    UserDialog userDialog = userDialogRepo.findUserDialogById(command.getUserId());

                    if (userDialog != null) {
                        switch (command.getType()) {
                            case "accept":
                                userDialog.setUserOn(true);
                                break;
                            case "cancel":
                                userDialog.setUserOn(false);
                                break;
                            default:
                                break;
                        }
                        userDialogRepo.save(userDialog);
                    }
                }
            }
        });

        //Обработка события messages
        socket.on("messages:" + auth.getTokenData().getOperatorId(), new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                logger.info("event messages: " + args[0].toString());

                EventMessage eventMessage = null;
                try {
                    eventMessage = new ObjectMapper()
                            .readValue(args[0].toString(), EventMessage.class);
                } catch (JsonProcessingException e) {
                    logger.error("При десериализации: {} произошла ошибка: {}", args[0], e.getMessage());
                }

                UserDialog userDialog =
                        userDialogRepo.findUserDialogById(eventMessage.getUserId());

                if (userDialog != null) {

                    Message message = mapper.map(eventMessage, Message.class);
                    message.setCreateStamp(LocalDateTime.now(ZoneId.of("UTC")));
                    switch (eventMessage.getType()) {
                        case "operator":
                            message.setMsgCode(2);
                            if (userDialog.getOperId() != null) {
                                message.setOperator(operatorRepo.findById(userDialog.getOperId().intValue()));
                            }
                            break;
                        case "user":
                            message.setMsgCode(3);
                            break;
                        default:
                            break;
                    }
                }
            }
        });

        //Обработка события queue (очередь пользователей)
        socket.on("queue:espp", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                logger.info("event queue: " + args[0].toString());
                try {
                    List<UserInQueue> userInQueueList = new ObjectMapper()
                            .readValue(args[0].toString(), new TypeReference<List<UserInQueue>>(){});

                    for (UserInQueue userInQueue: userInQueueList) {

                        UserDialog userDialog =
                                userDialogRepo.findUserDialogById(userInQueue.getId());

                        if (userDialog == null) {
                            userDialog = getUserDialogFromUserInQueue(userInQueue);
                            userDialog = userDialogRepo.save(userDialog);
                        }

                        if (!userDialog.isUserOn()) {
                            integrationService.getConnectedUsersQueue().add(userDialog.getId());
                        }
                    }

                } catch (JsonProcessingException e) {
                    logger.error("При десериализации: {} произошла ошибка: {}", args[0], e.getMessage());
                }
            }
        });

        //Обработка события connectedUsers
        socket.on("connectedUsers:" + auth.getTokenData().getOperatorId(), new Emitter.Listener() {

            @Override
            public void call(Object... args) {

                logger.info("event connectedUsers: " + args[0].toString());

                if (!imInitialized) {
                    try {
                        List<ConnectedUser> connectedUserList = new ObjectMapper()
                                .readValue(args[0].toString(), new TypeReference<List<ConnectedUser>>(){});

                        if (!connectedUserList.isEmpty()) {

                            userDialogRepo.connectUsers(connectedUserList.stream()
                                    .map(ConnectedUser::getId).collect(Collectors.toList()));

                        }

                    } catch (JsonProcessingException e) {
                        logger.error("При десериализации: {} произошла ошибка: {}", args[0], e.getMessage());
                    }

                    imInitialized = true;
                }
            }
        });

    }

    private UserDialog getUserDialogFromUserInQueue(UserInQueue userInQueue) {

        UserDialog userDialog = mapper.map(userInQueue, UserDialog.class);
        userDialog.setFullName(userInQueue.getName());
        userDialog.setLastActivity(0);
        userDialog.setUserOn(false);
        userDialog.setInQueue(true);

        return userDialog;
    }*/
}
