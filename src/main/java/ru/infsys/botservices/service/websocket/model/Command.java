package ru.infsys.botservices.service.websocket.model;

import lombok.Data;

/**
 * Структура, полученная через WebSocket в событии commands
 */
@Data
public class Command {

    /**
     * Идентификатор пользователя
     */
    private int userId;

    /**
     * Тип события
     * return - пользователя вернули в очередь
     * accept - пользователя взяли в работу
     * cancel - пользователя отключили
     */
    private String type;

}
