package ru.infsys.botservices.service.websocket;

import static java.util.Collections.singletonMap;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import java.net.URI;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import lombok.Getter;
import okhttp3.OkHttpClient;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.infsys.botservices.AppConfig;
import ru.infsys.botservices.job.ScheduledTasks;
import ru.infsys.botservices.persistence.entity.Message;
import ru.infsys.botservices.persistence.entity.UserDialog;
import ru.infsys.botservices.persistence.repository.ConfigParamRepo;
import ru.infsys.botservices.persistence.repository.MessageRepo;
import ru.infsys.botservices.persistence.repository.OperatorRepo;
import ru.infsys.botservices.persistence.repository.UserDialogRepo;
import ru.infsys.botservices.service.chatbot.Auth;
import ru.infsys.botservices.service.chatbot.ChatbotService;
import ru.infsys.botservices.service.chatbot.model.UserAd;
import ru.infsys.botservices.service.espp.IntegrationService;
import ru.infsys.botservices.service.websocket.model.Command;
import ru.infsys.botservices.service.websocket.model.ConnectedUser;
import ru.infsys.botservices.service.websocket.model.EventMessage;
import ru.infsys.botservices.service.websocket.model.EventMessageWithFile;
import ru.infsys.botservices.service.websocket.model.UserInQueue;

/**
 * Сервисы подключения к socket.io и обработки событий вебсокета.
 */
@Service
public class WebSocketService {

    @Autowired
    Auth auth;

    @Autowired
    AppConfig appConfig;

    @Autowired
    UserDialogRepo userDialogRepo;

    @Autowired
    OperatorRepo operatorRepo;

    @Autowired
    MessageRepo messageRepo;

    @Autowired
    IntegrationService integrationService;

    @Autowired
    ChatbotService chatbotService;

    @Autowired
    ModelMapper mapper;

    @Autowired
    ConfigParamRepo configParamRepo;

    @Autowired
    ScheduledTasks scheduledTasks;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private static final String NO_OPER_MSG = "В данный момент нет свободных операторов";

    /**
     * Признак инициализации модуля интеграции. При старте приложения уставлен в false.
     * Необходим для обработки события connectedUsers только при запуске приложения.
     */
    private boolean imInitialized = false;

    @Getter
    private LocalDateTime lastEventTime = LocalDateTime.now(ZoneOffset.UTC);

    /*private static final boolean useFilter =
            Boolean.parseBoolean("${botservices.websocket.useFilter}");

    private static final String loginPattern = "${botservices.websocket.loginPattern}";*/

    private Socket socket;

    /**
     * Подключение к socket.io
     */
    private void socketConnect() {

        try {
            IO.Options options = IO.Options.builder()
                    .setAuth(singletonMap("token", auth.getToken()))
                    .build();

            //IO.Options options = new IO.Options();

            OkHttpClient okHttpClient = getOkHttpClient();
            IO.setDefaultOkHttpWebSocketFactory(okHttpClient);
            IO.setDefaultOkHttpCallFactory(okHttpClient);
            options.callFactory = okHttpClient;
            options.webSocketFactory = okHttpClient;
            options.builder().setAuth(singletonMap("token", auth.getToken())).build();
            //options.auth.put("token", auth.getToken());
            options.transports = new String[]{"websocket"};

            socket = IO.socket(URI.create(appConfig.getWebSocketURL()), options);
            socket.connect();

        } catch (Exception e) {
            logger.error("При получении токена произошла ошибка: {}", e.getStackTrace());
        }
    }

    /**
     * Инициализация соединения с socket.io, подписка на события и их обработка.
     */
    @PostConstruct
    public void runSocket() {

        socketConnect();


        socket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                logger.info("Подключено к socket.io");
            }
        });

        socket.on(Socket.EVENT_CONNECT_ERROR, new Emitter.Listener() {
            @Override
            public void call(Object... objects) {
                logger.error("Ошибка подключения к socket.io {}", objects[0].toString());
                try {
                    reconnect();
                } catch (Exception e) {
                    logger.error("Ошибка переподключения к socket.io {}", e.getMessage());
                }
            }
        });

        //Обработка события commands
        socket.off("commands:" + auth.getTokenData().getOperatorId());
        socket.on("commands:" + auth.getTokenData().getOperatorId(), new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                logger.info("event commands: " + args[0].toString());
                lastEventTime = LocalDateTime.now(ZoneOffset.UTC);

                Command command = null;
                try {
                    command = new ObjectMapper()
                            .readValue(args[0].toString(), Command.class);
                } catch (JsonProcessingException e) {
                    logger.error("При десериализации: {} произошла ошибка: {}", args[0], e.getMessage());
                }

                if (command.getType().equals("accept") || command.getType().equals("cancel")) {

                    UserDialog userDialog = userDialogRepo.findUserDialogById(command.getUserId());

                    if (userDialog != null) {
                        switch (command.getType()) {
                            case "accept":
                                userDialog.setUserOn(true);
                                break;
                            case "cancel":
                                userDialog.setUserOn(false);
                                break;
                            default:
                                break;
                        }
                        userDialogRepo.save(userDialog);
                    }
                }
            }
        });

        //Обработка события messages
        socket.off("messages:" + auth.getTokenData().getOperatorId());
        socket.on("messages:" + auth.getTokenData().getOperatorId(), new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                logger.info("event messages: " + args[0].toString());
                lastEventTime = LocalDateTime.now(ZoneOffset.UTC);

                EventMessage eventMessage = null;
                EventMessageWithFile eventMessageWithFile = null;
                try {
                    if (messageWithFile(args[0].toString())) {
                        eventMessageWithFile = new ObjectMapper()
                                .readValue(args[0].toString(), EventMessageWithFile.class);
                    } else {
                        eventMessage = new ObjectMapper()
                                .readValue(args[0].toString(), EventMessage.class);
                    }
                } catch (JsonProcessingException e) {
                    logger.error("При десериализации: {} произошла ошибка: {}", args[0], e.getMessage());
                }

                UserDialog userDialog = null;
                if (eventMessage != null) {
                    userDialog =
                            userDialogRepo.findUserDialogById(eventMessage.getUserId());
                } else if (eventMessageWithFile != null) {
                    userDialog =
                            userDialogRepo.findUserDialogById(eventMessageWithFile.getUserId());
                }

                if (userDialog != null) {

                    Message message = null;
                    if (eventMessage != null) {
                        message = mapper.map(eventMessage, Message.class);
                    }
                    if (eventMessageWithFile != null) {
                        message = new Message();
                        message.setUserId(eventMessageWithFile.getUserId());
                        //message.setMsgFile(getJsonString(eventMessageWithFile.getMessage().getFiles()));
                        message.setMsgFile(getJsonString(eventMessageWithFile.getMessage()));

                    }

                    message.setCreateStamp(LocalDateTime.now(ZoneId.of("UTC")));
                    message.setId(null);
                    String messageType = null;
                    if (eventMessage != null) {
                        messageType = eventMessage.getType();
                    } else if (eventMessageWithFile != null) {
                        messageType = eventMessageWithFile.getType();
                    }

                    switch (messageType) {
                        case "operator":
                            message.setMsgCode(2);
                            userDialog.setLastActivity(2);
                            userDialog.setLastActivityStamp(LocalDateTime.now(ZoneOffset.UTC));
                            if (userDialog.getOperId() != null) {
                                message.setOperator(operatorRepo.findById(userDialog.getOperId().intValue()));
                            }
                            break;
                        case "user":
                            message.setMsgCode(3);
                            userDialog.setLastActivity(1);
                            userDialog.setLastActivityStamp(LocalDateTime.now(ZoneOffset.UTC));
                            break;
                        default:
                            break;
                    }
                    messageRepo.save(message);
                    userDialogRepo.save(userDialog);
                }
            }
        });

        //Обработка события queue (очередь пользователей)
        socket.off("queue:espp");
        socket.on("queue:espp", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                logger.info("event queue: " + args[0].toString());
                lastEventTime = LocalDateTime.now(ZoneOffset.UTC);

                /*int dlgMaxCount = Integer.parseInt(
                        configParamRepo.findByParamName("dlgMaxCount").getParamValue());*/

                try {
                    List<UserInQueue> userInQueueList = new ObjectMapper()
                            .readValue(args[0].toString(), new TypeReference<List<UserInQueue>>(){});

                    for (UserInQueue userInQueue: userInQueueList) {

                        /*try {
                            Integer operId = scheduledTasks
                                    .getAvailableOperId(integrationService.getOperStateMapById(), dlgMaxCount);
                            *//*if (operId == null) {
                                chatbotService.sendMessageToUser(userInQueue.getId(), NO_OPER_MSG);
                                chatbotService.cancelUser(userInQueue.getId());
                            }*//*
                        } catch (NoSuchElementException e) {

                            //chatbotService.sendMessageToUser(userInQueue.getId(), NO_OPER_MSG);
                            //chatbotService.cancelUser(userInQueue.getId());
                            continue;
                        }*/

                        //Если включен фильтр, то пропускать обработку пользователей
                        //с логином не соответствующему шаблону
                        if (appConfig.isUseFilter()
                                && !Pattern.compile(appConfig.getLoginPattern())
                                .matcher(userInQueue.getLogin()).find()) {

                            continue;
                        }

                        UserDialog userDialog =
                                userDialogRepo.findUserDialogById(userInQueue.getId());

                        if (userDialog == null) {
                            userDialog = getUserDialogFromUserAd(userInQueue.getLogin());
                        } else {
                            userDialog = setInQueueAttributes(userDialog);
                        }

                        integrationService.getConnectedUsersQueue()
                                .add(userDialogRepo.save(userDialog).getId());

                    }

                } catch (JsonProcessingException e) {
                    logger.error("При десериализации: {} произошла ошибка: {}", args[0], e.getMessage());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        //Обработка события connectedUsers
        socket.off("connectedUsers:" + auth.getTokenData().getOperatorId());
        socket.on("connectedUsers:" + auth.getTokenData().getOperatorId(), new Emitter.Listener() {

            @Override
            public void call(Object... args) {

                logger.info("event connectedUsers: " + args[0].toString());
                lastEventTime = LocalDateTime.now(ZoneOffset.UTC);

                if (!imInitialized) {
                    try {
                        List<ConnectedUser> connectedUserList = new ObjectMapper()
                                .readValue(args[0].toString(), new TypeReference<List<ConnectedUser>>(){});

                        if (!connectedUserList.isEmpty()) {

                            userDialogRepo.connectUsers(connectedUserList.stream()
                                    .map(ConnectedUser::getId).collect(Collectors.toList()));

                        }

                    } catch (JsonProcessingException e) {
                        logger.error("При десериализации: {} произошла ошибка: {}", args[0], e.getMessage());
                    }

                    imInitialized = true;
                }
            }
        });
    }

    private UserDialog getUserDialogFromUserInQueue(UserInQueue userInQueue) {

        UserDialog userDialog = mapper.map(userInQueue, UserDialog.class);
        userDialog.setFullName(userInQueue.getName());
        //При создании нового диалога необходимо установить значение LastActivityStamp не null,
        // т.к. по данному полю выполняется сортировка при распределении диалогов по операторам.
        userDialog.setLastActivityStamp(LocalDateTime.now(ZoneOffset.UTC));

        return setInQueueAttributes(userDialog);
    }

    private UserDialog getUserDialogFromUserAd(String userLogin) {

        UserDialog userDialog = null;

        try {
            UserAd userAd = chatbotService.getUserAdInfo(userLogin, false);
            userDialog = mapper.map(userAd, UserDialog.class);
            userDialog.setFullName(userAd.getSecondName() + " "
                                + userAd.getFirstName() + " "
                                + userAd.getThirdName());

            userDialog = setInQueueAttributes(userDialog);

        } catch (Exception e) {
            logger.error("Произошла ошибка {} при запросе пользователя ЕСК", e.getMessage());
        }

        return userDialog;
    }

    /**
     * Устанавливает аттрибуты userDialog, обозначающие, что диалог находится в очереди
     * на взятие в работу оператором.
     * @param userDialog - диалог пользователя
     * @return - userDialog
     */
    private UserDialog setInQueueAttributes(UserDialog userDialog) {
        userDialog.setLastActivity(0);
        userDialog.setLastActivityStamp(LocalDateTime.now(ZoneOffset.UTC));
        userDialog.setUserOn(false);
        userDialog.setInQueue(true);

        return userDialog;
    }

    public static OkHttpClient getOkHttpClient() {

        try {

            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, new TrustManager[]{new X509TrustManager() {
                @Override
                public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {

                }

                @Override
                public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {

                }

                @Override
                public X509Certificate[] getAcceptedIssuers() {
                    return new X509Certificate[0];
                }
            } }, new java.security.SecureRandom());

            OkHttpClient.Builder builder = new OkHttpClient.Builder();

            builder.hostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });

            builder.sslSocketFactory(sc.getSocketFactory(), new X509TrustManager() {
                @Override
                public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {

                }

                @Override
                public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {

                }

                @Override
                public X509Certificate[] getAcceptedIssuers() {
                    return new X509Certificate[0];
                }
            });

            return builder.build();

        } catch (NoSuchAlgorithmException | KeyManagementException ex) {
            ex.printStackTrace();
        }

        return null;
    }

    public void reconnect() {

        //auth.tokenInit();
        socket.disconnect();
        //socket.close();
        //socket = null;

        //socketConnect();
        runSocket();
        logger.info("Выполнено переподключение к socket.io");
    }

    public boolean getSocketState() {

        return socket.connected();
    }

    private boolean messageWithFile(String json) {

        JsonNode node = null;
        try {
            node = new ObjectMapper().readTree(json);
            return node.path("message").has("files");
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return false;
    }

    private String getJsonString(Object object) {
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        try {
            return ow.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }
}
