package ru.infsys.botservices.service.websocket.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

/**
 * Структура, полученная через WebSocket в событии connectedUsers (подключенные пользователи)
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ConnectedUser {

    /**
     * Идентификатор пользователя в системе.
     */
    private int id;

    /**
     * ФИО пользователя.
     */
    private String name;

    /**
     * Логин пользователя в домене.
     */
    private String login;

}
