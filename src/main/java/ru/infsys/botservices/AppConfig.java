package ru.infsys.botservices;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource(value = "application.yml", encoding = "UTF-8")
public class AppConfig {

    /**
     * Корневой URL REST сервисов ПО «Чат-бот».
     */
    @Getter
    @Value("${botservices.chat-bot.webSocketURL}")
    private String webSocketURL;

    /**
     * Корневой URL REST сервисов интерфейса оператора.
     */
    @Getter
    @Value("${botservices.chat-bot.operatorURL}")
    private String operatorURL;

    /**
     * Название системы, зарегистрированное в ПО «Чат-бот».
     */
    @Getter
    @Value("${botservices.chat-bot.systemName}")
    private String systemName;

    /**
     * Наименование учетной записи, от имени которой работает модуль интеграции.
     */
    @Getter
    @Value("${botservices.chat-bot.account}")
    private String account;

    /**
     * Пароль для шифрования.
     */
    @Getter
    @Value("${botservices.chat-bot.encryptionPsw}")
    private String encryptionPsw;

    /**
     * Время жизни токена доступа к модулю интеграции (в часах).
     */
    @Getter
    @Value("${botservices.chat-bot.tokenLifeLimit}")
    private int tokenLifeLimit;

    /**
     * Предельное время ожидания активности от АРМ Оператора (в минутах).
     */
    @Getter
    @Value("${botservices.chat-bot.activityWhiteLimit}")
    private int activityWhiteLimit;

    /**
     * Интервал выполнения регулярных работ (в секундах).
     */
    @Getter
    @Value("${botservices.chat-bot.regularWorkInterval}")
    private int regularWorkInterval;

    /**
     * #Интервал выполнения действий, связанных с распределением диалогов по операторам (в секундах).
     */
    @Getter
    @Value("${botservices.chat-bot.operAllocInterval}")
    private int operAllocInterval;

    /**
     * Интервал времени синхронизации с LDAP (в часах).
     */
    @Getter
    @Value("${botservices.chat-bot.ldapSyncInterval}")
    private String ldapSyncInterval;

    /**
     * Время ожидания свободного оператора (в минутах).
     */
    @Getter
    @Value("${botservices.chat-bot.noOperTimeOut}")
    private String noOperTimeOut;

    /**
     * Сообщение пользователю при отсутствии свободных операторов.
     */
    @Getter
    @Value("${botservices.chat-bot.noOperMsg}")
    private String noOperMsg;

    /**
     * Использовать ли фильтр при обработке очереди пользователей.
     */
    @Getter
    @Value("${botservices.websocket.useFilter}")
    private boolean useFilter;
    //Boolean.parseBoolean("${botservices.websocket.useFilter}");

    /**
     * В случае, если "useFilter = true", будут обрабатываться только логины пользователей, соответствующие шаблону.
     */
    @Getter
    @Value("${botservices.websocket.loginPattern}")
    private String loginPattern;// = "${botservices.websocket.loginPattern}";


    /**
     * Интервал проверки последнего события, полученного через WebSocket
     */
    @Getter
    @Value("${botservices.websocket.checkSocketEventsInterval}")
    private int checkSocketEventsInterval;

    /**
     * Если в течение данного периода не будет событий, полученных через WebSocket,
     * то будет выполнено переподключение к WebSocket (в минутах)
     */
    @Getter
    @Value("${botservices.websocket.noEventsPeriod}")
    private long noEventsPeriod;

    /**
     * Интервал проверки состояния Web Socket (подключено или нет)
     */
    @Getter
    @Value("${botservices.websocket.checkSocketStateInterval}")
    private int checkSocketStateInterval;

    /**
     * URL-адрес LDAP.
     */
    /*@Getter
    @Value("${botservices.ldap.ldapUrl}")
    private String ldapUrl;*/

    /**
     * Имя домена.
     */
    /*@Getter
    @Value("${botservices.ldap.domain}")
    private String domain;*/

    /**
     * Группа в Active Directory. Членам данной группы разрешен доступ к консоли администрирования чат-бот.
     */
    /*@Getter
    @Value("${botservices.ldap.ldapGroup}")
    private String ldapGroup;*/
}
