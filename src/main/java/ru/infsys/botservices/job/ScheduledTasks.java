package ru.infsys.botservices.job;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import ru.infsys.botservices.AppConfig;
import ru.infsys.botservices.persistence.entity.Operator;
import ru.infsys.botservices.persistence.entity.UserDialog;
import ru.infsys.botservices.persistence.repository.ConfigParamRepo;
import ru.infsys.botservices.persistence.repository.MessageRepo;
import ru.infsys.botservices.persistence.repository.OperatorRepo;
import ru.infsys.botservices.persistence.repository.UserDialogRepo;
import ru.infsys.botservices.service.chatbot.ChatbotService;
import ru.infsys.botservices.service.chatbot.model.UserAd;
import ru.infsys.botservices.service.espp.IntegrationService;
import ru.infsys.botservices.service.espp.model.MsgToSendInfo;
import ru.infsys.botservices.service.espp.model.OperState;
import ru.infsys.botservices.service.websocket.WebSocketService;

/**
 * Набор заданий, запускающихся по заданному интервалу.
 */
@Component
public class ScheduledTasks {

    @Autowired
    AppConfig appConfig;

    @Autowired
    OperatorRepo operatorRepo;

    @Autowired
    UserDialogRepo userDialogRepo;

    @Autowired
    MessageRepo messageRepo;

    @Autowired
    ChatbotService chatbotService;

    @Autowired
    IntegrationService integrationService;

    @Autowired
    ConfigParamRepo configParamRepo;

    @Autowired
    ModelMapper mapper;

    @Autowired
    WebSocketService webSocketService;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private static final String DISCONNECT_MSG =
            "Диалог будет приостановлен по причине продолжительного отсутствия ответа от Пользователя";

    private static final String regularWorkInterval =
            "${botservices.chat-bot.regularWorkInterval:5000}";

    private static final String operAllocInterval =
            "${botservices.chat-bot.operAllocInterval:5000}";

    private static final String checkSocketEventsInterval =
            "${botservices.websocket.checkSocketEventsInterval:5000}";

    private static final String checkSocketStateInterval =
            "${botservices.websocket.checkSocketStateInterval:5000}";

    /*private static final String noOperTimeOut =
            "${botservices.chat-bot.noOperTimeOut}";

    private static final String noOperMsg =
            "${botservices.chat-bot.noOperMsg}";*/

    /**
     * Синхронизация атрибутов оператора с LDAP.
     */
    @Scheduled(cron = "${botservices.chat-bot.ldapSyncInterval}")
    public void operatorSync() {

        logger.info("Синхронизация атрибутов оператора запущена");

        LocalDateTime currentTime = LocalDateTime.now(ZoneId.of("UTC"));

        List<Operator> synchronizedOperatorList = new ArrayList<>();

        List<Operator> operatorList = operatorRepo.findOperatorByLdapSyncStampLessThanEqual(
                currentTime.minusHours(appConfig.getTokenLifeLimit()));

        for (Operator operator : operatorList) {

            try {

                UserAd userAd = chatbotService.getUserAdInfo(operator.getLogin(), true);

                if (userAd.getId() != 0) {

                    synchronizedOperatorList
                            .add(getOperatorFromUserAd(userAd, currentTime, operator.getChatBotSkill()));

                } else {
                    logger.error("Оператор {} не найден в ЕСК", operator.getLogin());
                }

            } catch (Exception e) {
                logger.error("Произошла ошибка {} при запросе пользователя ЕСК", e.getMessage());
            }

        }

        operatorRepo.saveAll(synchronizedOperatorList);

        logger.info("Синхронизация атрибутов оператора завершена");

    }

    /**
     * Синхронизация атрибутов пользователя в UserDialog с LDAP.
     */
    @Scheduled(cron = "${botservices.chat-bot.ldapSyncInterval}")
    public void userDialogSync() {

        logger.info("Синхронизация атрибутов пользователя запущена");

        LocalDateTime currentTime = LocalDateTime.now(ZoneId.of("UTC"));

        List<UserDialog> synchronizedUserDialogList = new ArrayList<>();

        List<UserDialog> userDialogList = userDialogRepo
                .findUserDialogByLdapSyncStampAndLastActivityStamp(
                        currentTime.minusHours(appConfig.getTokenLifeLimit()));

        for (UserDialog userDialog : userDialogList) {

            try {

                UserAd userAd = chatbotService.getUserAdInfo(userDialog.getLogin(), false);

                if (userAd.getId() != 0) {

                    synchronizedUserDialogList.add(
                            getUserDialogFromUserAd(userAd, currentTime));
                } else {
                    logger.error("Пользователь {} не найден в ЕСК", userDialog.getLogin());
                }

            } catch (Exception e) {
                logger.error("Произошла ошибка {} при запросе пользователя ЕСК", e.getMessage());
            }
        }

        userDialogRepo.saveAll(synchronizedUserDialogList);

        logger.info("Синхронизация атрибутов пользователя завершена");

    }

    private Operator getOperatorFromUserAd(UserAd userAd, LocalDateTime currentTime, Boolean chatBotSkill) {

        Operator operator = mapper.map(userAd, Operator.class);
        operator.setFullName(getFullName(userAd));
        operator.setLdapSyncStamp(currentTime);
        operator.setChatBotSkill(chatBotSkill);

        return operator;
    }

    private UserDialog getUserDialogFromUserAd(UserAd userAd, LocalDateTime currentTime) {

        UserDialog userDialog = mapper.map(userAd, UserDialog.class);
        userDialog.setFullName(getFullName(userAd));
        userDialog.setLdapSyncStamp(currentTime);

        return userDialog;
    }

    private String getFullName(UserAd userAd) {
        return userAd.getSecondName() + " " + userAd.getFirstName() + " " + userAd.getThirdName();
    }

    /**
     * Распределение диалогов по операторам.
     */
    @Scheduled(fixedDelayString = operAllocInterval)
    public synchronized void allocDialog() {

        logger.info("Распределение диалогов по операторам запущено");

        int dlgMaxCount = Integer.parseInt(
                configParamRepo.findByParamName("dlgMaxCount").getParamValue());

        Map<Integer, OperState> operStateMap = integrationService.getOperStateMapById();

        //Отключить пользователей у которых время ожидания оператора превышено
        List<UserDialog> disconnectUserDialogList = userDialogRepo
                .findUserDialogsByUserOnTrueAndInQueueTrue();

        LocalDateTime disconnectTime = LocalDateTime.now(ZoneId.of("UTC"))
                .minusMinutes(Long.parseLong(appConfig.getNoOperTimeOut()));
        for (UserDialog userDialog : disconnectUserDialogList) {
            if (disconnectTime.isAfter(userDialog.getLastActivityStamp())) {
                try {
                    userDialog.setUserOn(false);
                    userDialog.setInQueue(false);
                    userDialog.setLastActivity(0);
                    userDialogRepo.save(userDialog);
                    chatbotService.sendMessageToUser(userDialog.getId(), appConfig.getNoOperMsg());
                    chatbotService.cancelUser(userDialog.getId());
                } catch (Exception e) {
                    logger.debug("Ошибка при отключении пользователя {}", e.getMessage());
                }
            }
        }

        if (operStateMap.isEmpty()) {
            logger.warn("На текущий момент нет подключенных операторов к модулю интеграции");
            return;
        }

        try {
            getAvailableOperId(operStateMap, dlgMaxCount);
        } catch (NoSuchElementException e) {
            logger.warn("На текущий момент нет доступных операторов");
            return;
        }

        OperState operState;

        List<UserDialog> userDialogList = userDialogRepo
                .findUserDialogsByUserOnTrueAndInQueueTrue();

        //Сперва сортируем диалоги по времени последней активности (сверху самые давние)
        userDialogList.sort(Comparator.comparing(UserDialog::getLastActivityStamp));

        for (UserDialog userDialog : userDialogList) {

            logger.debug("userDialog: " + userDialog.toString());

            Integer availableOperId = null;

            Integer operId = userDialog.getOperId();

            //Если в диалоге уже заполнен operId
            if (operId != null) {

                operState = operStateMap.get(operId);
                //logger.debug("operState: " + operState.toString());

                //То проверяем, что данный оператор доступен
                // и что его активные диалоги не превышают максимальное значение
                if (operState != null && operState.isAvailable() && operState.getActiveDialogs() < dlgMaxCount) {

                    //Если так, то исключаем диалог из очереди
                    // и увеличиваем количество активных диалогов оператора на 1.
                    userDialog.setInQueue(false);
                    integrationService.increaseActiveDialogs(operState.getId());
                    logger.debug("operState#3: " + operState.toString());

                } else {
                    //Иначе находим другого доступного оператора
                    availableOperId = getAvailableOperId(operStateMap, dlgMaxCount);
                }
            } else {

                //Если в диалоге не заполнен operId, то сразу ищем доступного оператора
                if (!operStateMap.isEmpty()) {
                    availableOperId = getAvailableOperId(operStateMap, dlgMaxCount);
                }
            }
            if (availableOperId != null) {
                userDialog.setOperId(availableOperId);
                userDialog.setInQueue(false);

                operState = operStateMap.get(availableOperId);
                integrationService.increaseActiveDialogs(operState.getId());
                logger.debug("operState#4: " + operState.toString());
            }

            userDialog.setLastActivityStamp(LocalDateTime.now(ZoneId.of("UTC")));
            userDialogRepo.save(userDialog);

        }
        logger.info("Распределение диалогов по операторам завершено");
    }

    /**
     * Получить идентификатор оператора с наименьшим количеством активных диалогов.
     *
     * @param operStateMap - HashMap состояний оператора (OperState) по идентификатору оператора.
     * @param dlgMaxCount  - максимально возможное количество одновременно обрабатываемых оператором диалогов
     *                     (параметр хранится в таблице ConfigParam).
     * @return - возвращает идентификатор оператора.
     */
    public Integer getAvailableOperId(Map<Integer, OperState> operStateMap, int dlgMaxCount) {

        return operStateMap.entrySet().stream()
                .filter(e -> e.getValue().isAvailable()
                        && e.getValue().getActiveDialogs() < dlgMaxCount)
                .min(Comparator.comparing(e -> e.getValue().getActiveDialogs()))
                .orElseThrow(NoSuchElementException::new).getKey();
    }

    /**
     * Подключение пользователей к модулю интеграции средствами API Чат-бота
     */
    @Scheduled(fixedDelayString = regularWorkInterval)
    public void connectUserToIntegrationModule() {
        logger.info("Подключение пользователей к модулю интеграции запущено");

        int dlgMaxCount = Integer.parseInt(
                        configParamRepo.findByParamName("dlgMaxCount").getParamValue());

        for (int userId : integrationService.getConnectedUsersQueue()) {
            try {
                Integer operId = getAvailableOperId(integrationService.getOperStateMapById(), dlgMaxCount);
                chatbotService.acceptUser(userId, 0);
                userDialogRepo.connectUser(userId);
                integrationService.getConnectedUsersQueue().remove(userId);
                messageRepo.saveAll(integrationService.getNewMessages(userId));

            } catch (NoSuchElementException nse) {
                continue;
            } catch (Exception e) {
                if (e.getMessage().contains("пользователь уже в работе")) {
                    integrationService.getConnectedUsersQueue().remove(userId);
                }
                if (e.getMessage().contains("Ошибка при взятии пользователя в работу")) {
                    UserDialog userDialog = userDialogRepo.findUserDialogById(userId);
                    userDialog.setUserOn(false);
                    userDialog.setInQueue(false);
                    userDialogRepo.save(userDialog);
                }
                logger.error("Ошибка при подключении пользователя к модулю интеграции {} ", e.getStackTrace());
            }
        }
        logger.info("Подключение пользователей к модулю интеграции завершено");
    }

    /**
     * Отключение пользователей от модуля интеграции средствами API Чат-бота
     */
    @Scheduled(fixedDelayString = regularWorkInterval)
    public void disconnectUserFromIntegrationModule() {

        logger.info("Отключение пользователей от модуля интеграции запущено");

        for (int userId : integrationService.getDisconnectedUsersQueue()) {

            logger.debug("DisconnectedUsersQueue#1: " + integrationService.getDisconnectedUsersQueue().toString());

            //перед отключением пользователя, проверяем есть ли неотправленные сообщения
            if (integrationService.getSendMsgQueue().stream().anyMatch(m -> m.getUserId() == userId)) {
                continue;
            }

            try {
                chatbotService.cancelUser(userId);
                //userDialogRepo.cancelUser(userId);
                integrationService.getDisconnectedUsersQueue().remove(userId);
            } catch (Exception e) {
                logger.error("Ошибка при отключении пользователя от модуля интеграции {} ", e.getStackTrace());
            }
        }
        logger.info("Отключение пользователей от модуля интеграции завершено");
    }

    /**
     * Отправка сообщений пользователям средствами API Чат-бота
     */
    @Scheduled(fixedDelayString = regularWorkInterval)
    public void sendMessage() {

        logger.info("Отправка сообщений пользователям запущено");
        for (MsgToSendInfo msgToSendInfo : integrationService.getSendMsgQueue()) {
            try {
                chatbotService.sendMessageToUser(msgToSendInfo.getUserId(), msgToSendInfo.getMsg());
                integrationService.getSendMsgQueue().remove(msgToSendInfo);
            } catch (Exception e) {
                logger.error("Ошибка при отправке сообщения пользователю {} ", e.getStackTrace());
            }
        }
        logger.info("Отправка сообщений пользователям завершено");
    }

    /**
     * Отключение неактивных пользователей
     */
    @Scheduled(fixedDelayString = regularWorkInterval)
    public void disconnectInactiveUsers() {

        logger.info("Отключение неактивных пользователей запущено");
        List<UserDialog> userDialogList = userDialogRepo.findInactiveUserDialogs(2,
                LocalDateTime.now(ZoneId.of("UTC"))
                        .minusMinutes(Long.parseLong(
                                configParamRepo.findByParamName("UserAnswerTimeout").getParamValue())));

        //List<UserDialog> updatedUserDialogList = new ArrayList<>();

        for (UserDialog userDialog : userDialogList) {

            /*userDialog.setInQueue(true);
            updatedUserDialogList.add(userDialog);*/
            try {
                chatbotService.sendMessageToUser(userDialog.getId(), DISCONNECT_MSG);
            } catch (Exception e) {
                logger.error("Ошибка при отправке сообщения пользователю: {}", e.getMessage());
            }

            integrationService.onCancelUser(userDialog.getId());
            integrationService.getDisconnectedUsersQueue().add(userDialog.getId());
            logger.debug("add DisconnectedUsersQueue#1: " + userDialog.toString());

            /*integrationService.getSendMsgQueue()
                    .add(new MsgToSendInfo(userDialog.getId(), DISCONNECT_MSG));*/

        }

        //userDialogRepo.saveAll(updatedUserDialogList);
        logger.info("Отключение неактивных пользователей завершено");
    }

    /**
     * Отключение неактивных операторов
     */
    @Scheduled(fixedDelayString = regularWorkInterval)
    public synchronized void disconnectInactiveOperators() {

        logger.info("Отключение неактивных операторов запущено");

        if (integrationService.getOperStateMapById().isEmpty()) {
            logger.warn("На текущий момент нет подключенных операторов к модулю интеграции");
            return;
        }

        List<UserDialog> userDialogList = userDialogRepo.findInactiveUserDialogs(1,
                LocalDateTime.now(ZoneId.of("UTC"))
                        .minusMinutes(Long.parseLong(
                                configParamRepo.findByParamName("OperAnswerTimeout").getParamValue())));

        List<UserDialog> updatedUserDialogList = new ArrayList<>();

        for (UserDialog userDialog : userDialogList) {

            OperState operState = null;

            if (userDialog.getOperId() != null) {

                operState = integrationService.getOperStateMapById().get(userDialog.getOperId());

                if (operState == null) {
                    continue;
                }
            }

            integrationService.decreaseActiveDialogs(operState.getId());
            logger.debug("operState#5: " + operState.toString());

            userDialog.setOperId(null);
            userDialog.setInQueue(true);
            updatedUserDialogList.add(userDialog);

        }

        userDialogRepo.saveAll(updatedUserDialogList);
        logger.info("Отключение неактивных операторов завершено");
    }

    @Scheduled(fixedDelayString = checkSocketEventsInterval)
    public void checkLastEventTime() {

        long noEventsMinutes = Duration
                .between(
                        webSocketService.getLastEventTime(),
                        LocalDateTime.now(ZoneOffset.UTC))
                .toMinutes();

        //logger.info("Время последнего события {} ", webSocketService.getLastEventTime());
        //logger.info("Текущее время {} ", LocalDateTime.now(ZoneOffset.UTC));

        logger.info("Не было событий в течении {} минут", noEventsMinutes);

        if (noEventsMinutes > appConfig.getNoEventsPeriod()) {
            webSocketService.reconnect();
        }

    }

    @Scheduled(fixedDelayString = checkSocketStateInterval)
    public void checkSocketState() {

        logger.info("SocketState: {}", webSocketService.getSocketState());
        //webSocketService.reconnect();
    }
}

/*
    private static final String checkSocketEventsInterval =
            "${botservices.websocket.checkSocketEventsInterval:5000}";

    private static final String checkSocketStateInterval =
            "${botservices.websocket.checkSocketStateInterval:5000}";
 */